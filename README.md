# PasteSpider

#### 介绍
PasteSpider社区开源版，如果你的服务器是小内存的，比如才2GB，那么这一款从开发角度触发的容器部署工具绝对适合你！
支持一键部署，Nginx路由同步，多环境发布，账号多角色，私有仓库,GIT/SVN源码发布等！


#### 安装教程

0.到<a href='https://soft.pastecode.cn/Home/Soft'>https://soft.pastecode.cn/Home/Soft</a>去打包最新的镜像 

具体的安装教程见 https://soft.pastecode.cn/Home/spider/8

<img src='https://soft.pastecode.cn/upload/page/0/c2c4761626d0ef30711aad83b9ed6acf_size.png'>

1.  需要安装Postgresql数据库

``
podman run -it --name postgres --restart always -e POSTGRES_PASSWORD=12345678 -e ALLOW_IP_RANGE=0.0.0.0/0 -v /outdata/postgres/data:/var/lib/postgresql/data -p 8765:5432 -d postgres
``

2.  需要安装redis缓存

``
docker run -it --name redis -p 8004:6379 -d --restart always redis --requirepass yourpassword
``

3.  使用docker或者Podman运行

``
docker run -it -v "/Users/apeart/spider/:/spider/" -e ConnectionStrings:MainConnectionString="User id=postgres;Password=12345678;Host=192.168.0.29;Port=8002;Database=spiderdb;Pooling=true;MaxPoolSize=10;MinPoolSize=0;Connection Lifetime=0;" -e RedisConfig:Mainconnection="192.168.0.29:8004,password=12345678,defaultdatabase=9" --name myspider -d imgspider:1021
``

## 升级部署
    
当在后台创建好项目和对应的服务后，后续升级一个服务就很简单了！支持一键升级也支持按照步骤的模式

<img src='https://soft.pastecode.cn/upload/page/0/b18611c4314b41c0f030ff2a40fb65db.png'>
