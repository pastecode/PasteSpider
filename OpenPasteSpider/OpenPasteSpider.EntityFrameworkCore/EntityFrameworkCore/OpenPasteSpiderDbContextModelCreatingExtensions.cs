﻿//using OpenPasteSpider.testmodel;
using System;
using Microsoft.EntityFrameworkCore;
using OpenPasteSpider.bind;
using OpenPasteSpider.monitormodel;
//using OpenPasteSpider.noticemodel;
using OpenPasteSpider.projectmodel;
using OpenPasteSpider.usermodel;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace OpenPasteSpider
{
    /// <summary>
    /// 
    /// </summary>
    public static class OpenPasteSpiderDbContextModelCreatingExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="optionsAction"></param>
        public static void ConfigureOpenPasteSpider(
            this ModelBuilder builder,
            Action<OpenPasteSpiderModelBuilderConfigurationOptions> optionsAction = null)
        {


            Check.NotNull(builder, nameof(builder));

            var options = new OpenPasteSpiderModelBuilderConfigurationOptions(
                OpenPasteSpiderDbProperties.DbTablePrefix,
                OpenPasteSpiderDbProperties.DbSchema
            );

            optionsAction?.Invoke(options);

            //**PlanInfo**
            builder.Entity<PlanInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "PlanInfo", options.Schema);
                b.ConfigureByConvention();
            });

            //**PlanItem**
            builder.Entity<PlanItem>(b =>
            {
                b.ToTable(options.TablePrefix + "PlanItem", options.Schema);
                b.ConfigureByConvention();
            });

            //**BindServiceImage**
            builder.Entity<BindServiceImage>(b =>
            {
                b.ToTable(options.TablePrefix + "BindServiceImage", options.Schema);
                b.ConfigureByConvention();
            });

            //**BindModelLinux**
            builder.Entity<BindModelLinux>(b =>
            {
                b.ToTable(options.TablePrefix + "BindModelLinux", options.Schema);
                b.ConfigureByConvention();
            });

            //**ServiceInfo**
            builder.Entity<ServiceInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "ServiceInfo", options.Schema);
                b.ConfigureByConvention();
            });

            //**NameInfo**
            builder.Entity<ProjectInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "NameInfo", options.Schema);
                b.ConfigureByConvention();
            });

            //**ModelInfo**
            builder.Entity<ModelInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "ModelInfo", options.Schema);
                b.ConfigureByConvention();
            });

            //**LinuxInfo**
            builder.Entity<LinuxInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "LinuxInfo", options.Schema);
                b.ConfigureByConvention();
            });

            //**AppInfo**
            builder.Entity<AppInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "AppInfo", options.Schema);
                b.ConfigureByConvention();
            });

            //**ProjectInfo**
            builder.Entity<ProjectInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "ProjectInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *UserInfo*
            builder.Entity<UserInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "UserInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *RoleInfo*
            builder.Entity<RoleInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "RoleInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *GradeInfo*
            builder.Entity<GradeInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "GradeInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *GradeRole*
            builder.Entity<GradeRole>(b =>
            {
                b.ToTable(options.TablePrefix + "GradeRole", options.Schema);
                b.ConfigureByConvention();
            });

            // *RevicerInfo*
            builder.Entity<RevicerInfo>(b =>
            {
                b.ToTable(options.TablePrefix + "RevicerInfo", options.Schema);
                b.ConfigureByConvention();
            });

            // *NoticeLog*
            builder.Entity<NoticeLog>(b =>
            {
                b.ToTable(options.TablePrefix + "NoticeLog", options.Schema);
                b.ConfigureByConvention();
            });

            //**UserAuth**
            builder.Entity<UserAuth>(b =>
            {
                b.ToTable(options.TablePrefix + "UserAuth", options.Schema);
                b.ConfigureByConvention();
            });

            //**DockerTimeState**
            builder.Entity<DockerTimeState>(b =>
            {
                b.ToTable(options.TablePrefix + "DockerTimeState", options.Schema);
                b.ConfigureByConvention();
            });

            //**DockerState**
            builder.Entity<DockerState>(b =>
            {
                b.ToTable(options.TablePrefix + "DockerState", options.Schema);
                b.ConfigureByConvention();
            });

            //**LinuxState**
            builder.Entity<LinuxState>(b =>
            {
                b.ToTable(options.TablePrefix + "LinuxState", options.Schema);
                b.ConfigureByConvention();
            });

            //**LinuxTimeState**
            builder.Entity<LinuxTimeState>(b =>
            {
                b.ToTable(options.TablePrefix + "LinuxTimeState", options.Schema);
                b.ConfigureByConvention();
            });
        }
    }
}