﻿using Microsoft.EntityFrameworkCore;
using OpenPasteSpider.bind;
using OpenPasteSpider.monitormodel;
using OpenPasteSpider.projectmodel;
using OpenPasteSpider.sourcemodel;
using OpenPasteSpider.usermodel;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace OpenPasteSpider
{
    /// <summary>
    /// 
    /// </summary>
    [ConnectionStringName(OpenPasteSpiderDbProperties.ConnectionStringName)]
    public class OpenPasteSpiderDbContext : AbpDbContext<OpenPasteSpiderDbContext>, IOpenPasteSpiderDbContext
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        public OpenPasteSpiderDbContext(DbContextOptions<OpenPasteSpiderDbContext> options) : base(options) { }

        /// <summary>
        /// 
        /// </summary>
        ~OpenPasteSpiderDbContext()
        {
            base.Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ConfigureOpenPasteSpider();
        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<AppInfo> AppInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<PlanInfo> PlanInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<PlanItem> PlanItem { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<BindServiceImage> BindServiceImage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<BindModelLinux> BindModelLinux { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ServiceInfo> ServiceInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ProjectInfo> ProjectInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbSet<ModelInfo> ModelInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<LinuxInfo> LinuxInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<AsyncConfig> AsyncConfig { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<UserInfo> UserInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<RoleInfo> RoleInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<GradeInfo> GradeInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<GradeRole> GradeRole { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<RevicerInfo> RevicerInfo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<NoticeLog> NoticeLog { get; set; }

        /// <summary>
        /// 用户授权
        /// </summary>
        public DbSet<UserAuth> UserAuth { get; set; }
        /// <summary>
        /// 小时数据
        /// </summary>
        public DbSet<DockerTimeState> DockerTimeState { get; set; }

        /// <summary>
        /// 五分钟数据
        /// </summary>
        public DbSet<DockerState> DockerState { get; set; }

        /// <summary>
        /// 五分钟数据
        /// </summary>
        public DbSet<LinuxState> LinuxState { get; set; }

        /// <summary>
        /// 1小时数据
        /// </summary>
        public DbSet<LinuxTimeState> LinuxTimeState { get; set; }
    }
}