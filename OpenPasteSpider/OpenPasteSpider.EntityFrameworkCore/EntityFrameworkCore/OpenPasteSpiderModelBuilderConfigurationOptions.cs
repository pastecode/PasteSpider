﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace OpenPasteSpider
{
    /// <summary>
    /// 
    /// </summary>
    public class OpenPasteSpiderModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tablePrefix"></param>
        /// <param name="schema"></param>
        public OpenPasteSpiderModelBuilderConfigurationOptions(
            [NotNull] string tablePrefix = "",
            [CanBeNull] string schema = null)
            : base(
                tablePrefix,
                schema)
        {

        }
    }
}