﻿using System;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.EntityFrameworkCore;
using Volo.Abp.Modularity;


namespace OpenPasteSpider
{
    /// <summary>
    /// 
    /// </summary>
    [DependsOn(
        typeof(OpenPasteSpiderDomainModule),
        typeof(AbpEntityFrameworkCoreModule)
    )]
    public class OpenPasteSpiderEntityFrameworkCoreModule : AbpModule
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Console.WriteLine("--- OpenPasteSpiderEntityFrameworkCoreModule.ConfigureServices ---");

            //var Configuration = context.Services.GetConfiguration();
            //var configlist = Configuration.AsEnumerable();
            //var result = configlist.Where(x => x.Key.StartsWith("SpiderConfig:")).ToDictionary(x => x.Key.Replace("SpiderConfig:", ""), x => x.Value);
            //var config = Newtonsoft.Json.JsonConvert.DeserializeObject<SpiderConfig>(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            Console.WriteLine(context.Services.GetConfiguration().GetConnectionString("MainConnectionString"));

            //pgsql
            context.Services.AddTransient<IOpenPasteSpiderDbContext,OpenPasteSpiderDbContext>();
            context.Services.AddAbpDbContext<OpenPasteSpiderDbContext>(op =>
            {
                Configure<AbpDbContextOptions>(aa => { aa.UseNpgsql(); });
            });
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            context.Services.AddAbpDbContext<OpenPasteSpiderDbContext>(options =>
            {
                options.AddDefaultRepositories<OpenPasteSpiderDbContext>(includeAllEntities: true);
            });

            

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            Console.WriteLine("--- OpenPasteSpiderEntityFrameworkCoreModule.PreConfigureServices ---");

            base.PreConfigureServices(context);
        }
    }
}