﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace OpenPasteSpider.EntityFrameworkCore.Migrations
{
    public partial class initdatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AsyncConfig",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ServiceId = table.Column<int>(type: "integer", nullable: false, comment: "服务ID"),
                    FileName = table.Column<string>(type: "text", nullable: true, comment: "文件或者文件夹全名称"),
                    IsDirectory = table.Column<bool>(type: "boolean", nullable: false, comment: "是否文件夹"),
                    Ignore = table.Column<bool>(type: "boolean", nullable: false, comment: "忽略或者必须"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "状态")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AsyncConfig", x => x.Id);
                },
                comment: "同步过滤");

            migrationBuilder.CreateTable(
                name: "PKAppInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "容器名称"),
                    AppID = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: true, comment: "简略ID"),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "创建时间"),
                    Address = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "容器IP"),
                    OutPort = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "映射端口"),
                    StateCode = table.Column<int>(type: "integer", nullable: false, comment: "运行状态"),
                    Version = table.Column<int>(type: "integer", nullable: false, comment: "镜像版本"),
                    ModelId = table.Column<int>(type: "integer", nullable: false, comment: "环境ID"),
                    ServiceId = table.Column<int>(type: "integer", nullable: false, comment: "服务ID"),
                    LinuxId = table.Column<int>(type: "integer", nullable: false, comment: "服务器ID"),
                    ProjectId = table.Column<int>(type: "integer", nullable: false, comment: "项目ID")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKAppInfo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PKBindModelLinux",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ModelId = table.Column<int>(type: "integer", nullable: false),
                    ServiceId = table.Column<int>(type: "integer", nullable: false),
                    ModelCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true),
                    LinuxId = table.Column<int>(type: "integer", nullable: false),
                    RunNum = table.Column<int>(type: "integer", nullable: false, comment: "当前运行量"),
                    LimitMinNum = table.Column<int>(type: "integer", nullable: false, comment: "最小运行量"),
                    LimitMaxNum = table.Column<int>(type: "integer", nullable: false, comment: "最大运行量"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "状态"),
                    Weight = table.Column<int>(type: "integer", nullable: false, comment: "分配权重")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKBindModelLinux", x => x.Id);
                },
                comment: "模块运行分布");

            migrationBuilder.CreateTable(
                name: "PKBindServiceImage",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ServiceId = table.Column<int>(type: "integer", nullable: false),
                    StoreId = table.Column<int>(type: "integer", nullable: false),
                    Version = table.Column<int>(type: "integer", nullable: false, comment: "版本号"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "是否可用"),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "创建时间"),
                    ImageDigest = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: true, comment: "镜像SHA256码"),
                    BuildState = table.Column<int>(type: "integer", nullable: false, comment: "0待构建 1构建成功 4构建失败 5取消构建")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKBindServiceImage", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PKGradeInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "角色分组名称"),
                    Desc = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "分组描述"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "状态")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKGradeInfo", x => x.Id);
                },
                comment: "角色信息");

            migrationBuilder.CreateTable(
                name: "PKGradeRole",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    GradeId = table.Column<int>(type: "integer", nullable: false, comment: "组别ID"),
                    RoleId = table.Column<int>(type: "integer", nullable: false, comment: "权限ID")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKGradeRole", x => x.Id);
                },
                comment: "角色绑定权限");

            migrationBuilder.CreateTable(
                name: "PKLinuxInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "服务器名称"),
                    LinuxName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "机器名称，用于host"),
                    SlaveName = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "分组名称"),
                    RemoteIP = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "外网IP地址"),
                    LocalIP = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: false, comment: "本机IP地址"),
                    BindNodeState = table.Column<int>(type: "integer", nullable: false, comment: "绑定状态"),
                    OpenPorts = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "开放端口，支持(1200-1300,1230,2345)写法"),
                    AcceptExpand = table.Column<bool>(type: "boolean", nullable: false, comment: "是否接受扩容"),
                    BoolBuild = table.Column<bool>(type: "boolean", nullable: false, comment: "是否是构建服务器"),
                    BuildHost = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "构建访问地址"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "是否可用"),
                    WorkDir = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true, comment: "工作目录"),
                    NginxDir = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "nginx的配置文件目录"),
                    SSHAddress = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "内部传输文件的IP地址"),
                    SSHPort = table.Column<int>(type: "integer", nullable: false, comment: "远程端口"),
                    SSHUser = table.Column<string>(type: "character varying(24)", maxLength: 24, nullable: true, comment: "远程账号"),
                    SSHPass = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "远程密码"),
                    SSHCertBody = table.Column<string>(type: "text", nullable: true, comment: "远程登录证书内容"),
                    SSHCertPass = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true, comment: "证书的密码，如果有"),
                    Unix = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "系统版本"),
                    Tool = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "容器类型"),
                    LinkToken = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true, comment: "链接密钥"),
                    LinkCode = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "链接专用码"),
                    FreeMemory = table.Column<int>(type: "integer", nullable: false, comment: "保留内存")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKLinuxInfo", x => x.Id);
                },
                comment: "服务器信息");

            migrationBuilder.CreateTable(
                name: "PKNoticeLog",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "发送时间"),
                    Code = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "消息代码"),
                    ObjId = table.Column<int>(type: "integer", nullable: false, comment: "对象ID"),
                    Body = table.Column<string>(type: "text", nullable: true, comment: "消息内容"),
                    Time = table.Column<int>(type: "integer", nullable: false, comment: "发生频次"),
                    RevicerId = table.Column<int>(type: "integer", nullable: false, comment: "接收者"),
                    Success = table.Column<bool>(type: "boolean", nullable: false, comment: "推送结果")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKNoticeLog", x => x.Id);
                },
                comment: "消息推送记录");

            migrationBuilder.CreateTable(
                name: "PKPlanInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OrderModel = table.Column<int>(type: "integer", nullable: false, comment: "任务类型"),
                    State = table.Column<int>(type: "integer", nullable: false, comment: "状态"),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "创建时间"),
                    Durtion = table.Column<int>(type: "integer", nullable: false, comment: "执行时长"),
                    LinuxId = table.Column<int>(type: "integer", nullable: false, comment: "执行服务器"),
                    ServiceId = table.Column<int>(type: "integer", nullable: false, comment: "目标服务ID"),
                    ModelId = table.Column<int>(type: "integer", nullable: false, comment: "目标模板ID"),
                    AppId = table.Column<int>(type: "integer", nullable: false, comment: "目标Container"),
                    NginxId = table.Column<int>(type: "integer", nullable: false, comment: "路由文件ID"),
                    StoreId = table.Column<int>(type: "integer", nullable: false, comment: "目标仓库"),
                    ProjectId = table.Column<int>(type: "integer", nullable: false, comment: "目标项目"),
                    Version = table.Column<int>(type: "integer", nullable: false, comment: "目标版本号")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKPlanInfo", x => x.Id);
                },
                comment: "总任务");

            migrationBuilder.CreateTable(
                name: "PKPlanItem",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PlanInfoId = table.Column<int>(type: "integer", nullable: false, comment: "任务ID"),
                    ActionType = table.Column<int>(type: "integer", nullable: false, comment: "动作名称"),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "创建时间"),
                    ExecState = table.Column<int>(type: "integer", nullable: false, comment: "执行状态"),
                    Command = table.Column<string>(type: "text", nullable: true, comment: "执行命令"),
                    RunIndex = table.Column<int>(type: "integer", nullable: false, comment: "执行顺序"),
                    ActionResult = table.Column<string>(type: "text", nullable: true, comment: "执行的结果"),
                    Target = table.Column<string>(type: "text", nullable: true, comment: "参数或目标"),
                    StrictMode = table.Column<bool>(type: "boolean", nullable: false, comment: "严格模式"),
                    Durtion = table.Column<int>(type: "integer", nullable: false, comment: "运行耗时"),
                    ExitCode = table.Column<int>(type: "integer", nullable: false, comment: "退出代码"),
                    AppId = table.Column<int>(type: "integer", nullable: false, comment: "容器")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKPlanItem", x => x.Id);
                },
                comment: "任务详细");

            migrationBuilder.CreateTable(
                name: "PKProjectInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "代码"),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "名称"),
                    NotifyUrl = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true, comment: "变更推送地址"),
                    Desc = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "描述"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "状态")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKProjectInfo", x => x.Id);
                },
                comment: "项目");

            migrationBuilder.CreateTable(
                name: "PKRevicerInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "接收者名称"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "状态是否可用"),
                    Url = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true, comment: "推送地址"),
                    Codes = table.Column<string>(type: "text", nullable: true, comment: "接收消息类型"),
                    TotalSend = table.Column<int>(type: "integer", nullable: false, comment: "推送次数"),
                    TotalFailed = table.Column<int>(type: "integer", nullable: false, comment: "推送失败次数"),
                    ProjectId = table.Column<int>(type: "integer", nullable: false, comment: "项目ID")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKRevicerInfo", x => x.Id);
                },
                comment: "消息推送接收者");

            migrationBuilder.CreateTable(
                name: "PKRoleInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Model = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "权限模块"),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "此项的名称"),
                    Role = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "权限值"),
                    Desc = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "描述"),
                    FatherId = table.Column<int>(type: "integer", nullable: false, comment: "父级ID"),
                    RoleType = table.Column<int>(type: "integer", nullable: false, comment: "权限类型"),
                    FatherStr = table.Column<string>(type: "text", nullable: true, comment: "层级簇"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "状态"),
                    Sort = table.Column<int>(type: "integer", nullable: false, comment: "排序")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKRoleInfo", x => x.Id);
                },
                comment: "权限列表");

            migrationBuilder.CreateTable(
                name: "PKUserAuth",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<int>(type: "integer", nullable: false, comment: "用户ID"),
                    ServiceId = table.Column<int>(type: "integer", nullable: false, comment: "服务ID"),
                    ModelCodes = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "环境代码集"),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "创建日期"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "状态")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKUserAuth", x => x.Id);
                },
                comment: "用户授权");

            migrationBuilder.CreateTable(
                name: "PKUserInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true, comment: "用户名"),
                    CreateDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "创建日期"),
                    Desc = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "描述"),
                    Email = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "邮箱"),
                    PassWord = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "密码"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "状态"),
                    Grade = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "哪个角色")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKUserInfo", x => x.Id);
                },
                comment: "管理账号信息");

            migrationBuilder.CreateTable(
                name: "PKServiceInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "代码"),
                    Name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "名称"),
                    Desc = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "服务的详细描述"),
                    DirectPath = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "直接目录,上传文件用"),
                    IsEnable = table.Column<bool>(type: "boolean", nullable: false, comment: "是否可用"),
                    Version = table.Column<int>(type: "integer", nullable: false, comment: "当前镜像版本号"),
                    FileVersion = table.Column<int>(type: "integer", nullable: false, comment: "文件版本号"),
                    ConfigNotify = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "配置推送地址"),
                    HealthPath = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true, comment: "检查地址"),
                    ListenPorts = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: true, comment: "监听内部端口"),
                    NeedMapping = table.Column<bool>(type: "boolean", nullable: false, comment: "是否需要端口映射"),
                    FileModel = table.Column<int>(type: "integer", nullable: false, comment: "文件模式"),
                    DirectImage = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true, comment: "采用的镜像名称"),
                    BeforeCommands = table.Column<string>(type: "text", nullable: true, comment: "前置命令"),
                    AutoRestart = table.Column<bool>(type: "boolean", nullable: false, comment: "自动重启"),
                    AfterCommands = table.Column<string>(type: "text", nullable: true, comment: "后置命令"),
                    UpdateSplits = table.Column<int>(type: "integer", nullable: false, comment: "分几份升级"),
                    SaveNumber = table.Column<int>(type: "integer", nullable: false, comment: "保存多少个版本"),
                    OtherArgs = table.Column<string>(type: "text", nullable: true, comment: "docker run附带参数"),
                    EnvironmentArgs = table.Column<string>(type: "text", nullable: true, comment: "docker run 附带的环境参数，在名称后面"),
                    ProjectId = table.Column<int>(type: "integer", nullable: true),
                    NumBuild = table.Column<int>(type: "integer", nullable: false, comment: "构建镜像次数"),
                    NumBuildFailed = table.Column<int>(type: "integer", nullable: false, comment: "构建镜像失败次数")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKServiceInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PKServiceInfo_PKProjectInfo_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "PKProjectInfo",
                        principalColumn: "Id");
                },
                comment: "服务信息");

            migrationBuilder.CreateTable(
                name: "PKModelInfo",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Code = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true, comment: "环境代码"),
                    Desc = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true, comment: "环境描述"),
                    NumResize = table.Column<int>(type: "integer", nullable: false, comment: "伸缩次数"),
                    NumFailedResize = table.Column<int>(type: "integer", nullable: false, comment: "伸缩失败次数"),
                    NumUpdate = table.Column<int>(type: "integer", nullable: false, comment: "升级次数"),
                    NumFailedUpdate = table.Column<int>(type: "integer", nullable: false, comment: "升级失败次数"),
                    ServiceId = table.Column<int>(type: "integer", nullable: true),
                    ProjectId = table.Column<int>(type: "integer", nullable: false, comment: "项目ID")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKModelInfo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PKModelInfo_PKServiceInfo_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "PKServiceInfo",
                        principalColumn: "Id");
                },
                comment: "环境信息");

            migrationBuilder.CreateIndex(
                name: "IX_PKGradeInfo_Name",
                table: "PKGradeInfo",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PKModelInfo_ServiceId",
                table: "PKModelInfo",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_PKServiceInfo_Name",
                table: "PKServiceInfo",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_PKServiceInfo_ProjectId",
                table: "PKServiceInfo",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AsyncConfig");

            migrationBuilder.DropTable(
                name: "PKAppInfo");

            migrationBuilder.DropTable(
                name: "PKBindModelLinux");

            migrationBuilder.DropTable(
                name: "PKBindServiceImage");

            migrationBuilder.DropTable(
                name: "PKGradeInfo");

            migrationBuilder.DropTable(
                name: "PKGradeRole");

            migrationBuilder.DropTable(
                name: "PKLinuxInfo");

            migrationBuilder.DropTable(
                name: "PKModelInfo");

            migrationBuilder.DropTable(
                name: "PKNoticeLog");

            migrationBuilder.DropTable(
                name: "PKPlanInfo");

            migrationBuilder.DropTable(
                name: "PKPlanItem");

            migrationBuilder.DropTable(
                name: "PKRevicerInfo");

            migrationBuilder.DropTable(
                name: "PKRoleInfo");

            migrationBuilder.DropTable(
                name: "PKUserAuth");

            migrationBuilder.DropTable(
                name: "PKUserInfo");

            migrationBuilder.DropTable(
                name: "PKServiceInfo");

            migrationBuilder.DropTable(
                name: "PKProjectInfo");
        }
    }
}
