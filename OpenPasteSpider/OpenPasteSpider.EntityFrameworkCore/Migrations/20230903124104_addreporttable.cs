﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace OpenPasteSpider.EntityFrameworkCore.Migrations
{
    public partial class addreporttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PKDockerState",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    AppShortId = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: true, comment: "Container.ID"),
                    AppInfoId = table.Column<int>(type: "integer", nullable: false, comment: "AppInfo.Id"),
                    ModelCode = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: true, comment: "哪个环境"),
                    ServiceId = table.Column<int>(type: "integer", nullable: false, comment: "哪个服务"),
                    DataDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false, comment: "数据时间yyyy"),
                    DockerName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true, comment: "容器名称"),
                    MemoryUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false, comment: "内存占用MB"),
                    CPUUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false, comment: "CPU数据"),
                    PIDSNum = table.Column<int>(type: "integer", nullable: false, comment: "线程数量"),
                    LinuxId = table.Column<int>(type: "integer", nullable: false, comment: "哪一台服务器的数据"),
                    NetIn = table.Column<decimal>(type: "numeric(18,2)", nullable: false, comment: "MB单位流量入"),
                    NetOut = table.Column<decimal>(type: "numeric(18,2)", nullable: false, comment: "MB单位流量出")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKDockerState", x => x.Id);
                },
                comment: "容器的状态记录");

            migrationBuilder.CreateTable(
                name: "PKDockerTimeState",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DataDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DockerName = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    MemoryUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    CPUUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    PIDSNum = table.Column<int>(type: "integer", nullable: false),
                    LinuxId = table.Column<int>(type: "integer", nullable: false),
                    HCPU = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    LCPU = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    HMemory = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    LMemory = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    HPIDS = table.Column<int>(type: "integer", nullable: false),
                    LPIDS = table.Column<int>(type: "integer", nullable: false),
                    NetIn = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    NetOut = table.Column<decimal>(type: "numeric(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKDockerTimeState", x => x.Id);
                },
                comment: "容器的小时状态数据");

            migrationBuilder.CreateTable(
                name: "PKLinuxState",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LinuxId = table.Column<int>(type: "integer", nullable: false),
                    DataDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CPUUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    CPU1 = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    CPU2 = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    CPU3 = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    MemoryUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    MemoryFree = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    MemoryTotal = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    NetIn = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    NetOut = table.Column<decimal>(type: "numeric(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKLinuxState", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PKLinuxTimeState",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    LinuxId = table.Column<int>(type: "integer", nullable: false),
                    DataDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CPUUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    CPU1 = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    CPU2 = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    CPU3 = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    MemoryUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    HMemeoryUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    LMemoryUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    HCPUUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    LCPUUsed = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    MemoryFree = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    MemoryTotal = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    NetIn = table.Column<decimal>(type: "numeric(18,2)", nullable: false),
                    NetOut = table.Column<decimal>(type: "numeric(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PKLinuxTimeState", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PKDockerState_DataDate",
                table: "PKDockerState",
                column: "DataDate");

            migrationBuilder.CreateIndex(
                name: "IX_PKDockerTimeState_DataDate",
                table: "PKDockerTimeState",
                column: "DataDate");

            migrationBuilder.CreateIndex(
                name: "IX_PKLinuxState_DataDate",
                table: "PKLinuxState",
                column: "DataDate");

            migrationBuilder.CreateIndex(
                name: "IX_PKLinuxTimeState_DataDate",
                table: "PKLinuxTimeState",
                column: "DataDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PKDockerState");

            migrationBuilder.DropTable(
                name: "PKDockerTimeState");

            migrationBuilder.DropTable(
                name: "PKLinuxState");

            migrationBuilder.DropTable(
                name: "PKLinuxTimeState");
        }
    }
}
