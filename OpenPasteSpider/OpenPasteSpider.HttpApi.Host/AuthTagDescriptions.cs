﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace OpenPasteSpider
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthTagDescriptions : IDocumentFilter
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="swaggerDoc"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
        {
            swaggerDoc.Tags = new List<OpenApiTag>
            {
                new OpenApiTag{ Name="AppInfo",Description="运行的Container"},
                new OpenApiTag{ Name="LinuxInfo",Description="服务器列表"},
                new OpenApiTag{ Name="ServiceInfo",Description="服务列表"},
                new OpenApiTag{ Name="ConfigInfo",Description="服务器列表"},
                new OpenApiTag{ Name="ModelInfo",Description="模式信息"},
                new OpenApiTag{ Name="PlanInfo",Description="计划信息"},
                new OpenApiTag{ Name="ProjectInfo",Description="项目信息"}
            };
        }

    }
}
