﻿using System;
//using Castle.Core.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace OpenPasteSpider
{
    /// <summary>
    /// 
    /// </summary>
    public class Program
    {

        /// <summary>
        /// 
        /// </summary>
        public static IConfiguration Configuration { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static string Development()
        {
            return Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static int Main(string[] args)
        {

            var builder = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.developer.json", optional: true)
                .AddEnvironmentVariables();//是否从环境变量中读取值
            Configuration = builder.Build();

            

            //开启日志
            Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(Configuration).CreateLogger();
            if (!System.IO.Directory.Exists("data"))
            {
                System.IO.Directory.CreateDirectory("data");
            }
            if (!System.IO.Directory.Exists("tempfile"))
            {
                System.IO.Directory.CreateDirectory("tempfile");
            }

            Console.WriteLine("Program:ConnectionConfig:");
            Console.WriteLine(Configuration.GetValue<string>("ConnectionStrings:MainConnectionString"));
            Console.WriteLine(Configuration.GetValue<string>("RedisConfig:MainConnection"));

            try
            {
                Log.Information("Starting web host.");
                CreateHostBuilder(args, 0).Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly!");
                return 1;
            }
            finally
            {
               
                Log.CloseAndFlush();
            }
        }

        internal static IHostBuilder CreateHostBuilder(string[] args, int port) =>
            Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            })
            .UseSerilog()
            .UseAutofac();
    }
}
