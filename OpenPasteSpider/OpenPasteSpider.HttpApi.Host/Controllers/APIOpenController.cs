﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenPasteSpider.usermodel;
using Z.EntityFramework.Plus;

namespace OpenPasteSpider.Controllers
{
    /// <summary>
    /// 公开接口，需要根据apitoken获取必要的查询
    /// </summary>
    [ApiController]
    [Route("/api/spider/open/[action]")]
    public class APIOpenController : IBaseController
    {

        private readonly IOpenPasteSpiderDbContext _dbContext;
        private readonly ICacheHelper _cache;
        private readonly ILogger<APIUserController> _logger;
        private readonly ImageHelper _imageHelper;

        private readonly SpiderConfig _config;
        /// <summary>
        /// 初始化
        /// </summary>
        public APIOpenController(IOpenPasteSpiderDbContext dbContext,
            ICacheHelper cache,
            ILogger<APIUserController> logger,
            IOptions<SpiderConfig> config,
            ImageHelper imageHelper)
        {
            _dbContext = dbContext;
            _cache = cache;
            _imageHelper = imageHelper;
            _logger = logger;
            _config = config.Value;
        }


        //查询某一个项目的某一个服务的某一个环境的所有运行app 缓冲事件... .. .

        //需要验证 _config.usertoken headers.xtoken中


    }

}
