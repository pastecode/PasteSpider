﻿using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace OpenPasteSpider.Controllers
{
    /// <summary>
    /// 公开接口，需要根据apitoken获取必要的查询
    /// </summary>
    [ApiController]
    [Route("/api/spider/code/[action]")]
    public class APIModelController : IBaseController
    {

        private readonly IOpenPasteSpiderDbContext _dbContext;
        /// <summary>
        /// 初始化
        /// </summary>
        public APIModelController(IOpenPasteSpiderDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// 环境信息的单选
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string single()
        {
            //var codes = await _dbContext.ModelConfig.Where(x => x.IsEnable).OrderBy(x => x.Sort).AsNoTracking().ToListAsync();
            //if (codes != null && codes.Count > 0)
            //{
            var sbstr = new StringBuilder();
            //foreach (var item in codes)
            //{

            sbstr.AppendLine($"<option value=\"default\">默认环境</option>");

            //}
            return sbstr.ToString();
            //}
            //else
            //{
            //    return "环境信息配置错误！";
            //}
        }

        /// <summary>
        /// 环境信息的多选
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string multiple()
        {
            //var codes = await _dbContext.ModelConfig.Where(x => x.IsEnable).OrderBy(x => x.Sort).AsNoTracking().ToListAsync();
            //if (codes != null && codes.Count > 0)
            //{
            var sbstr = new StringBuilder();
            //foreach (var item in codes)
            //{
            sbstr.AppendLine($"<li value=\"default\">默认环境</li>");
            //}
            return sbstr.ToString();
            //}
            //else
            //{
            //    return "环境信息配置错误！";
            //}
        }

    }

}
