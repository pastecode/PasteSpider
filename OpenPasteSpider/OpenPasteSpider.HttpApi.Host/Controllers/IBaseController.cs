﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc;

namespace OpenPasteSpider.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class IBaseController : AbpController
    {
        /// <summary>
        /// 获取当前登录用户
        /// </summary>
        /// <returns></returns>
        protected int ReadLoginUserId()
        {
            var token = base.Request.Headers["token"].ToString();
            if (!string.IsNullOrEmpty(token))
            {
                var strs = token.Split("_");
                int.TryParse(strs[1], out var userid);
                return userid;
            }
            return 0;
        }
        /// <summary>
        /// 获取当前登录用户密钥
        /// </summary>
        /// <returns></returns>
        protected string ReadLoginUserToken()
        {
            var token = base.Request.Headers["token"].ToString();
            if (!string.IsNullOrEmpty(token))
            {
                return token;
            }
            return "";
        }

        /// <summary>
        /// 读取客户端IP
        /// </summary>
        /// <returns></returns>
        protected string ReadClientIP()
        {
            string result = String.Empty;
            result = base.Request.Headers["X-Forwarded-For"];
            if (String.IsNullOrEmpty(result))
            {
                result = base.HttpContext.Connection.RemoteIpAddress.ToString();
            }
            if (String.IsNullOrEmpty(result))
            {
                result = base.Request.Headers["REMOTE_ADDR"];
            }

            if (result != null)
            {
                if (result.Contains(","))
                {
                    result = result.Split(',')[0];
                }
                else if (result == "::1")
                {
                    result = "127.0.0.1";
                }
            }
            return result;
        }
    }
}
