﻿namespace OpenPasteSpider
{

    /// <summary>
    /// 竞选集群管理者信息
    /// </summary>
    public class SlaveVoteInfo
    {

        /// <summary>
        /// 当前时间戳 毫秒
        /// </summary>
        public long time { get; set; }

        /// <summary>
        /// 提交我的信息
        /// </summary>
        public SlaveNodeItem Master { get; set; }

    }

    /// <summary>
    /// 节点信息
    /// </summary>
    public class SlaveNodeItem
    {
        /// <summary>
        /// 节点ID
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 竞选时间
        /// </summary>
        public long time { get; set; }

        /// <summary>
        /// 地址信息
        /// </summary>
        public string url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public RunState Status { get; set; } = RunState.unknow;

        /// <summary>
        /// 
        /// </summary>
        public long LastLink { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TryTime { get; set; } = 5;

        /// <summary>
        /// slavename 节点分组名称
        /// </summary>
        public string slavename { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SlaveHealthModel
    {
        /// <summary>
        /// 节点的master的id
        /// </summary>
        public int masterid { get; set; }
        /// <summary>
        /// 节点的master的url
        /// </summary>
        public string masterurl { get; set; }
        /// <summary>
        /// 节点的master的vote时间
        /// </summary>
        public int mastertime { get; set; }
        /// <summary>
        /// 节点的id
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 节点的地址
        /// </summary>
        public string url { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class SlaveCallBack
    {
        /// <summary>
        /// 200 确认成功 213旧的还能用
        /// </summary>
        public int code { get; set; } = 200;
        /// <summary>
        /// 213的时候为SlaveMater.String
        /// </summary>
        public string message { get; set; } = "执行成功";
    }

    /// <summary>
    /// 
    /// </summary>
    public class StatusModel
    {
        /// <summary>
        /// 1task 2node
        /// </summary>
        public int ModelType { get; set; }

        /// <summary>
        /// 1+ 2update 3del
        /// </summary>
        public int Action { get; set; }

        /// <summary>
        /// 对象 taskinfodto/nodeinfodto
        /// </summary>
        public string body { get; set; }
    }

    /// <summary>
    /// 节点状态
    /// </summary>
    public enum SlaveState
    {
        /// <summary>
        /// 离线了
        /// </summary>
        offline = 1,
        /// <summary>
        /// 在集群中
        /// </summary>
        ingroup = 2,
        /// <summary>
        /// 在线
        /// </summary>
        online = 3,
        /// <summary>
        /// 引发异常
        /// </summary>
        exception = 4,
        /// <summary>
        /// 链接中
        /// </summary>
        linking = 5,
        /// <summary>
        /// 建立链接
        /// </summary>
        linked = 6
    }

    /// <summary>
    /// 服务器配置信息 从appsettings.json中读取，不入库，k9s.service需要读取部分，需要做automapped!
    /// </summary>
    public class SpiderConfig
    {

        /// <summary>
        /// 组别名称
        /// </summary>
        public string GroupName { get; set; } = "";

        ///// <summary>
        ///// 是否使用redis
        ///// </summary>
        //public bool UseRedis { get; set; } = true;

        /// <summary>
        /// 集群链接密钥
        /// </summary>
        public string SlaveToken { get; set; } = "";

        /// <summary>
        /// 资源回收时间，凌晨3点
        /// </summary>
        public int RecoveryHour { get; set; } = 3;

        /// <summary>
        /// 删除多少天前的资源 60天前
        /// </summary>
        public int RecoveryDay { get; set; } = 60;

        /// <summary>
        /// 系统版本号
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// pgsql mysql sqlite
        /// </summary>
        public string SqlDataType { get; set; } = "pgsql";

        /// <summary>
        /// 等待启动最大次数 2s一次
        /// </summary>
        public int WaitRunTime { get; set; } = 20;

        /// <summary>
        /// 等待nginx的加载时间
        /// </summary>
        public int WaitNginxLoadTime { get; set; } = 10;

        /// <summary>
        /// 映射工作目录 /app/work/
        /// </summary>
        public string WorkDir { get; set; } = "/app/work/";

        /// <summary>
        /// 测试镜像名称
        /// </summary>
        public string TestImage { get; set; } = "pastetestimage";

        /// <summary>
        /// 用户密钥 官网查看的个人密钥
        /// </summary>
        public string UserToken { get; set; } = "";

        /// <summary>
        /// 按用户构建或者直接构建都需要填写
        /// </summary>
        public string SyncSoftToken { get; set; } = "";

    }
}
