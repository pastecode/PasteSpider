﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.projectmodel
{
    /// <summary>
    /// Container docker的容器 启动先后顺序如何定义。。。 。。 。
    /// </summary>
    public class AppInfo : Entity<int>
    {
        /// <summary>
        /// 容器名称 model_id_randcode
        /// </summary>
        [Comment("容器名称")]
        [MaxLength(128)]
        public string Name { get; set; } = "";

        /// <summary>
        /// 简略ID 12
        /// </summary>
        [MaxLength(12)]
        [Comment("简略ID")]
        public string AppID { get; set; } = "";

        /// <summary>
        /// 创建时间
        /// </summary>
        [Comment("创建时间")]
        public DateTime CreateDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 容器IP,通过inspect获取 容器启动后，会执行命令去启动
        /// </summary>
        [MaxLength(16)]
        [Comment("容器IP")]
        public string Address { get; set; } = "";

        /// <summary>
        /// 映射端口 15000,16594,14002 可以空，表示用IP绑定
        /// </summary>
        [MaxLength(128)]
        [Comment("映射端口")]
        public string OutPort { get; set; } = "";

        /// <summary>
        /// 运行状态
        /// </summary>
        [Comment("运行状态")]
        public RunState StateCode { get; set; } = RunState.waitrun;

        /// <summary>
        /// 镜像版本
        /// </summary>
        [Comment("镜像版本")]
        public int Version { get; set; } = 1000;

        /// <summary>
        /// 环境ID
        /// </summary>
        [Comment("环境ID")]
        public int ModelId { get; set; }

        /// <summary>
        /// 服务ID
        /// </summary>
        [Comment("服务ID")]
        public int ServiceId { get; set; }

        /// <summary>
        /// 服务器ID
        /// </summary>
        [Comment("服务器ID")]
        public int LinuxId { get; set; } = 0;

        /// <summary>
        /// 项目ID
        /// </summary>
        [Comment("项目ID")]
        public int ProjectId { get; set; } = 0;

    }
}
