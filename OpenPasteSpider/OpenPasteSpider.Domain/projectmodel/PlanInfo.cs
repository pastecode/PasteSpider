﻿using System;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.projectmodel
{
    /// <summary>
    /// 总任务概述
    /// </summary>
    [Comment("总任务")]
    public class PlanInfo : Entity<int>
    {

        /// <summary>
        /// 任务类型
        /// </summary>
        [Comment("任务类型")]
        public OrderModel OrderModel { get; set; }

        /// <summary>
        /// 待执行 执行中 执行成功 执行失败 任务取消 任务超时
        /// </summary>
        [Comment("状态")]
        public RunState State { get; set; } = RunState.waitrun;

        /// <summary>
        /// 创建时间
        /// </summary>
        [Comment("创建时间")]
        public DateTime CreateDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 执行时长 s
        /// </summary>
        [Comment("执行时长")]
        public int Durtion { get; set; }

        /// <summary>
        /// 执行服务器
        /// </summary>
        [Comment("执行服务器")]
        public int LinuxId { get; set; }

        /// <summary>
        /// 目标服务ID
        /// </summary>
        [Comment("目标服务ID")]
        public int ServiceId { get; set; }

        /// <summary>
        /// 目标模板ID (对某一个服务的model进行 升级，缩减，等)
        /// </summary>
        [Comment("目标模板ID")]
        public int ModelId { get; set; }

        /// <summary>
        /// 目标Container (停止或者重启某一个container)
        /// </summary>
        [Comment("目标Container")]
        public int AppId { get; set; }

        /// <summary>
        /// 路由文件ID
        /// </summary>
        [Comment("路由文件ID")]
        public int NginxId { get; set; }

        /// <summary>
        /// 目标仓库
        /// </summary>
        [Comment("目标仓库")]
        public int StoreId { get; set; }

        /// <summary>
        /// 目标项目
        /// </summary>
        [Comment("目标项目")]
        public int ProjectId { get; set; }

        /// <summary>
        /// 目标版本号
        /// </summary>
        [Comment("目标版本号")]
        public int Version { get; set; }

        //谁发起的任务？
    }
}
