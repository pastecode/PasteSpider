﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.projectmodel
{
    /// <summary>
    /// 服务器信息
    /// </summary>
    [Comment("服务器信息")]
    public class LinuxInfo : Entity<int>
    {

        /// <summary>
        /// 服务器名称
        /// </summary>
        [Comment("服务器名称")]
        [MaxLength(16)]
        public string Name { get; set; } = "";

        /// <summary>
        /// 机器名称 用于Host指定
        /// </summary>
        [MaxLength(64)]
        [Comment("机器名称，用于host")]
        public string LinuxName { get; set; } = "";

        /// <summary>
        /// 组别名称 不需要管理端指定，由后台设定
        /// </summary>
        [MaxLength(16)]
        [Comment("分组名称")]
        public string SlaveName { get; set; } = "default";

        /// <summary>
        /// 外网IP信息
        /// </summary>
        [Comment("外网IP地址")]
        [MaxLength(16)]
        public string RemoteIP { get; set; } = "";

        /// <summary>
        /// 局域网的IP
        /// </summary>
        [Comment("本机IP地址")]
        [Required]
        [MaxLength(16)]
        public string LocalIP { get; set; } = "";

        /// <summary>
        /// 绑定到节点的状态 0未知 1绑定 2绑定中 4绑定失败
        /// </summary>
        [Comment("绑定状态")]
        public int BindNodeState { get; set; } = 0;

        /// <summary>
        /// 使用命令检查端口是否被占用了 netstat -tunlp |grep 80或者netstat -tunlp遍历结果
        /// </summary>
        [Comment("开放端口，支持(1200-1300,1230,2345)写法")]
        [MaxLength(128)]
        public string OpenPorts { get; set; } = "";

        /// <summary>
        /// 是否接受扩容新任务
        /// </summary>
        [Comment("是否接受扩容")]
        public bool AcceptExpand { get; set; } = true;

        /// <summary>
        /// 是否是构建服务器
        /// </summary>
        [Comment("是否是构建服务器")]
        public bool BoolBuild { get; set; } = false;

        /// <summary>
        /// 构建访问地址 需要外部可以访问
        /// </summary>
        [Comment("构建访问地址")]
        [MaxLength(128)]
        public string BuildHost { get; set; } = "";

        /// <summary>
        /// 状态
        /// </summary>
        [Comment("是否可用")]
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 工作目录 下有template(存放项目的nginx模板文件) backup(存放项目的备份xxx/version/*) code(如果需要服务端构建的话) publish(打包文件的地址,当前的版本)
        /// </summary>
        [Comment("工作目录")]
        [MaxLength(32)]
        public string WorkDir { get; set; } = "/spider/workdir/";

        /// <summary>
        /// NGinx的配置文件，一般为/etc/nginx/conf.d/
        /// </summary>
        [Comment("nginx的配置文件目录")]
        [MaxLength(128)]
        public string NginxDir { get; set; } = "/etc/nginx/conf.d/";

        /// <summary>
        /// SSHIP地址
        /// </summary>
        [MaxLength(16)]
        [Comment("内部传输文件的IP地址")]
        public string SSHAddress { get; set; } = "";

        /// <summary>
        /// 远程端口
        /// </summary>
        [Comment("远程端口")]
        public int SSHPort { get; set; } = 22;

        /// <summary>
        /// 远程账号 不配置远程文件同步和远程命令的话可以不需要配置
        /// </summary>
        [Comment("远程账号")]
        [MaxLength(24)]
        public string SSHUser { get; set; } = "root";

        /// <summary>
        /// 远程密码 不配置远程文件同步和远程命令的话可以不需要配置 内部加密下
        /// </summary>
        [Comment("远程密码")]
        [MaxLength(64)]
        public string SSHPass { get; set; } = "XOD03-43DFS-23423DSFS-234";

        /// <summary>
        /// 远程登录证书内容
        /// </summary>
        [Comment("远程登录证书内容")]
        public string SSHCertBody { get; set; } = "";

        /// <summary>
        /// 证书密码
        /// </summary>
        [MaxLength(32)]
        [Comment("证书的密码，如果有")]
        public string SSHCertPass { get; set; } = "";

        /// <summary>
        /// 系统版本 centos7 centos8 不同版本不同的命令，甚至不同的反馈结果 相当于要做一个兼容
        /// </summary>
        [Comment("系统版本")]
        [MaxLength(16)]
        public string Unix { get; set; } = "centos7";

        /// <summary>
        /// 容器类型 docker podman
        /// </summary>
        [Comment("容器类型")]
        [MaxLength(16)]
        public string Tool { get; set; } = "docker";

        /// <summary>
        /// 链接密钥
        /// </summary>
        [MaxLength(256)]
        [Comment("链接密钥")]
        public string LinkToken { get; set; } = "";

        /// <summary>
        /// 链接码
        /// </summary>
        [Comment("链接专用码")]
        [MaxLength(128)]
        public string LinkCode { get; set; } = "";

        /// <summary>
        /// 保留内存 多少MB的可用内存
        /// </summary>
        [Comment("保留内存")]
        public int FreeMemory { get; set; } = 500;
    }
}
