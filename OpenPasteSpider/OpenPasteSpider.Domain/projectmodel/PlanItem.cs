﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.projectmodel
{
    /// <summary>
    /// linux待执行命令
    /// </summary>
    [Comment("任务详细")]
    public class PlanItem:Entity<int>
    {
        /// <summary>
        /// 主任务ID
        /// </summary>
        [Comment("任务ID")]
        public int PlanInfoId { get; set; }

        /// <summary>
        /// 哪一个动作
        /// </summary>
        [Comment("动作名称")]
        public ActionState ActionType { get; set; } = ActionState.custom;

        /// <summary>
        /// 作废 创建时间
        /// </summary>
        [Comment("创建时间")]
        public DateTime CreateDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 执行状态
        /// </summary>
        [Comment("执行状态")]
        public RunState ExecState { get; set; } = RunState.waitrun;

        /// <summary>
        /// 执行命令 比如docker ps / docker stats -- no-stream
        /// </summary>
        [Comment("执行命令")]
        public string Command { get; set; } = "";

        /// <summary>
        /// 子命令的顺序，从小到大执行
        /// </summary>
        [Comment("执行顺序")]
        public int RunIndex { get; set; } = 0;

        /// <summary>
        /// 执行的反馈
        /// </summary>
        [Comment("执行的结果")]
        public string ActionResult { get; set; } = "";

        /// <summary>
        /// 目标值，或者参数
        /// </summary>
        [Comment("参数或目标")]
        public string Target { get; set; } = "";

        /// <summary>
        /// 严格模式 严格模式遇到错误，则后续的动作会不执行
        /// </summary>
        [Comment("严格模式")]
        public bool StrictMode { get; set; } = true;

        /// <summary>
        /// 耗时 s
        /// </summary>
        [Comment("运行耗时")]
        public int Durtion { get; set; } = 0;

        /// <summary>
        /// 退出代码 命令程序的退出代码，作为是否正确执行的一个依据
        /// </summary>
        [Comment("退出代码")]
        public int ExitCode { get; set; } = 0;

        /// <summary>
        /// ContainerId
        /// </summary>
        [Comment("容器")]
        public int AppId { get; set; } = 0;

    }
}
