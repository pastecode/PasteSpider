﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.projectmodel
{
    /// <summary>
    /// 环境信息
    /// </summary>
    [Comment("环境信息")]
    public class ModelInfo:Entity<int>
    {
        /// <summary>
        /// 环境代码 default/product/preview 8
        /// </summary>
        [MaxLength(8)]
        [Comment("环境代码")]
        public string Code { get; set; } = "default";

        /// <summary>
        /// 环境描述 32
        /// </summary>
        [MaxLength(32)]
        [Comment("环境描述")]
        public string Desc { get; set; } = "";

        /// <summary>
        /// 伸缩次数
        /// </summary>
        [Comment("伸缩次数")]
        public int NumResize { get; set; } = 0;

        /// <summary>
        /// 伸缩失败次数
        /// </summary>
        [Comment("伸缩失败次数")]
        public int NumFailedResize { get; set; } = 0;

        /// <summary>
        /// 升级次数
        /// </summary>
        [Comment("升级次数")]
        public int NumUpdate { get; set; } = 0;

        /// <summary>
        /// 升级失败次数
        /// </summary>
        [Comment("升级失败次数")]
        public int NumFailedUpdate { get; set; } = 0;

        /// <summary>
        /// 哪一个服务
        /// </summary>
        [Comment("服务")]
        public ServiceInfo Service { get; set; }

        /// <summary>
        /// 项目ID
        /// </summary>
        [Comment("项目ID")]
        public int ProjectId { get; set; } = 0;
    }
}
