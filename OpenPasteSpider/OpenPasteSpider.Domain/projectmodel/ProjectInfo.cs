﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.projectmodel
{
    /// <summary>
    /// 项目
    /// </summary>
    [Comment("项目")]
    public class ProjectInfo : Entity<int>
    {

        /// <summary>
        /// 项目代码 英文16
        /// </summary>
        [MaxLength(16)]
        [Comment("代码")]
        public string Code { get; set; } = "";

        /// <summary>
        /// 项目名称 中文16
        /// </summary>
        [MaxLength(16)]
        [Comment("名称")]
        public string Name { get; set; } = "";

        /// <summary>
        /// 变更后通知地址 可以自己在地址中附带token 会推送路由的消息体过去
        /// </summary>
        [MaxLength(256)]
        [Comment("变更推送地址")]
        public string NotifyUrl { get; set; } = "";

        /// <summary>
        /// 描述
        /// </summary>
        [MaxLength(128)]
        [Comment("描述")]
        public string Desc { get; set; } = "";

        /// <summary>
        /// 状态
        /// </summary>
        [Comment("状态")]
        public bool IsEnable { get; set; } = true;

    }
}
