﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.projectmodel
{

    /// <summary>
    /// 应用，服务信息
    /// </summary>
    [Index(nameof(Name), IsUnique = false)]
    [Comment("服务信息")]
    public class ServiceInfo : Entity<int>
    {

        /// <summary>
        /// 服务代码 16英文
        /// </summary>
        [MaxLength(16)]
        [Comment("代码")]
        public string Code { get; set; } = "default";

        /// <summary>
        /// 服务名称 16中文
        /// </summary>
        [MaxLength(16)]
        [Comment("名称")]
        public string Name { get; set; } = "";

        /// <summary>
        /// 服务的详细描述 128
        /// </summary>
        [MaxLength(128)]
        [Comment("服务的详细描述")]
        public string Desc { get; set; } = "";

        /// <summary>
        /// 直接目录，一般用于前端静态,需要在工作目录下
        /// </summary>
        [MaxLength(128)]
        [Comment("直接目录,上传文件用")]
        public string DirectPath { get; set; }

        /// <summary>
        /// 是否可用
        /// </summary>
        [Comment("是否可用")]
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 当前镜像版本，有没有更高的版本在BindImageService中查看
        /// </summary>
        [Comment("当前镜像版本号")]
        public int Version { get; set; } = 1000;

        /// <summary>
        /// 文件更新版本 每次同步加1
        /// </summary>
        [Comment("文件版本号")]
        public int FileVersion { get; set; } = 0;

        /// <summary>
        /// 配置变动推送地址 /api/config/update这个有歧义，应该放在项目那边
        /// </summary>
        [Comment("配置推送地址")]
        [MaxLength(128)]
        public string ConfigNotify { get; set; } = "/api/config/update";

        /// <summary>
        /// 检查地址 相对地址，用于检测是否启动完成！/api/app/health
        /// </summary>
        [Comment("检查地址")]
        [MaxLength(256)]
        public string HealthPath { get; set; } = "";

        /// <summary>
        /// 监听端口 多个用逗号隔开
        /// </summary>
        [MaxLength(128)]
        [Comment("监听内部端口")]
        public string ListenPorts { get; set; } = "80";

        /// <summary>
        /// 是否需要端口映射
        /// </summary>
        [Comment("是否需要端口映射")]
        public bool NeedMapping { get; set; } = false;

        /// <summary>
        /// 文件模式 0静态模式 1源码模式 2发布模式 3镜像模式
        /// </summary>
        [Comment("文件模式")]
        public int FileModel { get; set; } = 0;

        /// <summary>
        /// 镜像名称 当文件类型为镜像模式的时候使用这个xxx:xxx
        /// </summary>
        [MaxLength(256)]
        [Comment("采用的镜像名称")]
        public string DirectImage { get; set; } = "";

        /// <summary>
        /// 升级前置命令，一行一命令 支持{{Service.WorkDir}}等变量，需要一个文档说明 用什么换行呢？ svn git拉取 dotnet build / dotnet publish等
        /// </summary>
        [Comment("前置命令")]
        public string BeforeCommands { get; set; } = "";

        /// <summary>
        /// 是否自动重启
        /// </summary>
        [Comment("自动重启")]
        public bool AutoRestart { get; set; } = true;

        /// <summary>
        /// 升级后置命令
        /// </summary>
        [Comment("后置命令")]
        public string AfterCommands { get; set; } = "";

        /// <summary>
        /// 升级拆分至多几次，每次不得小于1 默认5次
        /// </summary>
        [Comment("分几份升级")]
        public int UpdateSplits { get; set; } = 5;

        /// <summary>
        /// 保存的版本号！
        /// </summary>
        [Comment("保存多少个版本")]
        [DefaultValue(5)]
        public int SaveNumber { get; set; } = 10;

        /// <summary>
        /// 其他参数 docker run的附带参数，前后没有空格 不能为null 可以是 -e -p -v等
        /// </summary>
        [Comment("docker run附带参数")]
        public string OtherArgs { get; set; } = "";

        /// <summary>
        /// 环境参数 node server.js arg1 arg2 arg3
        /// </summary>
        [Comment("docker run 附带的环境参数，在名称后面")]
        public string EnvironmentArgs { get; set; } = "";

        /// <summary>
        /// 项目信息
        /// </summary>
        [Comment("项目信息")]
        public ProjectInfo Project { get; set; }

        /// <summary>
        /// 构建次数
        /// </summary>
        [Comment("构建镜像次数")]
        public int NumBuild { get; set; }

        /// <summary>
        /// 构建失败次数
        /// </summary>
        [Comment("构建镜像失败次数")]
        public int NumBuildFailed { get; set; }
    }
}
