﻿
namespace OpenPasteSpider
{
    public static class OpenPasteSpiderDbProperties
    {
        public static string DbTablePrefix { get; set; } = "PK";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "MainConnectionString";

        public const string OnlyReadConnectionStringName = "ReadConnectionString";
    }
}
