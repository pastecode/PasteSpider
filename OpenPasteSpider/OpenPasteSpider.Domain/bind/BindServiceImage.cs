﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.projectmodel
{
    /// <summary>
    /// 服务的镜像版本
    /// </summary>
    public class BindServiceImage : Entity<int>
    {

        /// <summary>
        /// 属于哪一个服务的包
        /// </summary>
        public int ServiceId { get; set; }

        /// <summary>
        /// 使用的哪一个镜像库
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// 当前版本
        /// </summary>
        [Comment("版本号")]
        public int Version { get; set; }

        /// <summary>
        /// 是否可用
        /// </summary>
        [Comment("是否可用")]
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 构建时间
        /// </summary>
        [Comment("创建时间")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 文件SHA256码，用于删除
        /// </summary>
        [MaxLength(300)]
        [Comment("镜像SHA256码")]
        public string ImageDigest { get; set; }

        /// <summary>
        /// 0待构建 1构建成功 4构建失败 5取消构建
        /// </summary>
        [Comment("0待构建 1构建成功 4构建失败 5取消构建")]
        public RunState BuildState { get; set; }

    }

}
