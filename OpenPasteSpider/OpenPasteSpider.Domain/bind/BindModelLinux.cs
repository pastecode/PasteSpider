﻿using Microsoft.EntityFrameworkCore;
using OpenPasteSpider.projectmodel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.bind
{
    /// <summary>
    /// 模块运行在哪些服务器上，或者说允许运行在哪些服务器上
    /// </summary>
    [Comment("模块运行分布")]
    public class BindModelLinux : Entity<int>
    {
        /// <summary>
        /// 模型
        /// </summary>
        public int ModelId { get; set; }

        /// <summary>
        /// 服务ID
        /// </summary>
        public int ServiceId { get; set; }

        /// <summary>
        /// 环境名称
        /// </summary>
        [MaxLength(8)]
        public string ModelCode { get; set; }

        /// <summary>
        /// 服务器
        /// </summary>
        public int LinuxId { get; set; }

        /// <summary>
        /// 忽略 将作废 当前运行数
        /// </summary>
        [Comment("当前运行量")]
        public int RunNum { get; set; } = 0;

        /// <summary>
        /// 最小运行量
        /// </summary>
        [Comment("最小运行量")]
        public int LimitMinNum { get; set; } = 1;

        /// <summary>
        /// 最大运行量
        /// </summary>
        [Comment("最大运行量")]
        public int LimitMaxNum { get; set; } = 100;

        /// <summary>
        /// 状态
        /// </summary>
        [Comment("状态")]
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 权重 自动分配权限
        /// </summary>
        [Comment("分配权重")]
        public int Weight { get; set; } = 100;


    }

}
