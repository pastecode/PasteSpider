﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.monitormodel
{
    /// <summary>
    /// Docker的监控记录
    /// </summary>
    //[Index(nameof(AppPrefix),IsUnique =false)]
    [Comment("容器的状态记录")]
    [Index(nameof(DataDate), IsUnique = false)]
    public class DockerState : Entity<long>
    {
        ///// <summary>
        ///// 去除版本号后的名称 容器名称前缀
        ///// </summary>
        //[MaxLength(20)]
        //[DefaultValue("去除版本号后的名称")]
        //public string AppPrefix { get; set; }

        //project.service.model.app.id

        /// <summary>
        /// Container.ID
        /// </summary>
        [MaxLength(16)]
        [Comment("Container.ID")]
        public string AppShortId { get; set; } = "";

        /// <summary>
        /// AppInfo.Id
        /// </summary>
        [Comment("AppInfo.Id")]
        public int AppInfoId { get; set; }

        /// <summary>
        /// 哪个环境
        /// </summary>
        [MaxLength(8)]
        [Comment("哪个环境")]
        public string ModelCode { get; set; } = "";

        /// <summary>
        /// 哪个服务
        /// </summary>
        [Comment("哪个服务")]
        public int ServiceId { get; set; }

        /// <summary>
        /// 数据时间yyyy-MM-dd HH:mm:ss
        /// </summary>
        [Comment("数据时间yyyy")]
        public DateTime DataDate { get; set; }

        /// <summary>
        /// 容器名称
        /// </summary>
        [MaxLength(64)]
        [Comment("容器名称")]
        public string DockerName { get; set; } = "";

        /// <summary>
        /// 内存占用MB
        /// </summary>
        [Comment("内存占用MB")]
        [Column(TypeName = "decimal(18,2)")]
        public decimal MemoryUsed { get; set; }

        /// <summary>
        /// CPU数据
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        [Comment("CPU数据")]
        public decimal CPUUsed { get; set; }

        /// <summary>
        /// 线程数量
        /// </summary>
        [Comment("线程数量")]
        public int PIDSNum { get; set; }

        /// <summary>
        /// 哪一台服务器的数据
        /// </summary>
        [Comment("哪一台服务器的数据")]
        public int LinuxId { get; set; }

        /// <summary>
        /// MB单位流量入
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        [Comment("MB单位流量入")]
        public decimal NetIn { get; set; }

        /// <summary>
        /// MB单位流量出
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        [Comment("MB单位流量出")]
        public decimal NetOut { get; set; }

    }
}
