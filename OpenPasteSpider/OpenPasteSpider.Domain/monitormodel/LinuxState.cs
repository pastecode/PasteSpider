﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.monitormodel
{
    /// <summary>
    /// Linux机器的CPU和内存情况
    /// </summary>
    [Index(nameof(DataDate), IsUnique = false)]
    public class LinuxState : Entity<long>
    {
        /// <summary>
        /// 哪一台服务器的
        /// </summary>
        public int LinuxId { get; set; }

        /// <summary>
        /// 数据时间
        /// </summary>
        public DateTime DataDate { get; set; }

        /// <summary>
        /// CPU当前使用量或平均值
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal CPUUsed { get; set; }

        /// <summary>
        /// 1分钟CPU平均值
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal CPU1 { get; set; }

        /// <summary>
        /// 5分钟CPU平均值
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal CPU2 { get; set; }

        /// <summary>
        /// 15分钟CPU平均值
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal CPU3 { get; set; }

        /// <summary>
        /// 使用内存MB或平均值
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal MemoryUsed { get; set; }

        /// <summary>
        /// 空闲内存MB或平均值
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal MemoryFree { get; set; }

        /// <summary>
        /// 总内存MB固定值
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal MemoryTotal { get; set; }

        /// <summary>
        /// 网卡入流量
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal NetIn { get; set; }

        /// <summary>
        /// 网卡出流量
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal NetOut { get; set; }
    }


}
