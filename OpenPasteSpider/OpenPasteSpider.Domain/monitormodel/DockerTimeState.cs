﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.monitormodel
{
    /// <summary>
    /// 容器的小时状态统计数据
    /// </summary>
    [Comment("容器的小时状态数据")]
    [Index(nameof(DataDate), IsUnique = false)]
    public class DockerTimeState : Entity<long>
    {

        /// <summary>
        /// 数据时间yyyy-MM-dd HH:mm:ss
        /// </summary>
        public DateTime DataDate { get; set; }

        /// <summary>
        /// 容器名称
        /// </summary>
        [MaxLength(64)]
        public string DockerName { get; set; } = "";

        /// <summary>
        /// 内存占用MB
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal MemoryUsed { get; set; }

        /// <summary>
        /// CPU数据
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal CPUUsed { get; set; }

        /// <summary>
        /// 线程数量
        /// </summary>
        public int PIDSNum { get; set; }

        /// <summary>
        /// 哪一台服务器的数据
        /// </summary>
        public int LinuxId { get; set; }

        /// <summary>
        /// 最高CPU占用
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal HCPU { get; set; }

        /// <summary>
        /// 最低CPU占用
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal LCPU { get; set; }

        /// <summary>
        /// 最高内存占用
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal HMemory { get; set; }

        /// <summary>
        /// 最低内存占用
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal LMemory { get; set; }

        /// <summary>
        /// 最高线程数
        /// </summary>
        public int HPIDS { get; set; }

        /// <summary>
        /// 最低线程数
        /// </summary>
        public int LPIDS { get; set; }

        /// <summary>
        /// MB单位流量入
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal NetIn { get; set; }

        /// <summary>
        /// MB单位流量出
        /// </summary>
        [Column(TypeName = "decimal(18,2)")]
        public decimal NetOut { get; set; }

    }
}
