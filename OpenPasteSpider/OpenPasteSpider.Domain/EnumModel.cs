﻿namespace OpenPasteSpider
{
    /// <summary>
    /// 任务的类型
    /// </summary>
    public enum OrderModel
    {
        /// <summary>
        /// 扩展，添加container
        /// </summary>
        enlarge = 1,
        /// <summary>
        /// 缩减 减少container
        /// </summary>
        reduce = 2,
        /// <summary>
        /// 全部升级
        /// </summary>
        update = 3,
        /// <summary>
        /// 构建仓储
        /// </summary>
        addregistry = 5,
        /// <summary>
        /// 登陆到私有仓库
        /// </summary>
        loginregistry = 4,
        /// <summary>
        /// 重启container
        /// </summary>
        restart = 6,
        /// <summary>
        /// 停止container
        /// </summary>
        stop = 7,
        /// <summary>
        /// 构建镜像并推送
        /// </summary>
        buildimage = 8,
        /// <summary>
        /// 灰度升级
        /// </summary>
        updategray = 9,
        /// <summary>
        /// 服务器检查是否安装了特定软件
        /// </summary>
        checksoft = 10,
        /// <summary>
        /// 更新服务器的daemon.json 加入域名白名单才能拉取
        /// </summary>
        daemonupdate = 11,
        /// <summary>
        /// 伸缩
        /// </summary>
        resize=12
    }

    /// <summary>
    /// 运行状态
    /// </summary>
    public enum RunState
    {

        //待运行 运行中(没重启) 运行中(有重启) 预停止 已删除 

        /// <summary>
        /// 未知
        /// </summary>
        unknow=-1,
        /// <summary>
        /// 待运行
        /// </summary>
        waitrun = 0,
        /// <summary>
        /// 运行中
        /// </summary>
        running = 1,
        /// <summary>
        /// 已取消
        /// </summary>
        cannel = 2,
        /// <summary>
        /// 成功
        /// </summary>
        success = 3,
        /// <summary>
        /// 失败
        /// </summary>
        failed = 4,
        /// <summary>
        /// 错误
        /// </summary>
        error = 5,
        /// <summary>
        /// 已停止
        /// </summary>
        stop = 6,
        /// <summary>
        /// 预停止
        /// </summary>
        prestop = 10,
        /// <summary>
        /// 没有找到这个命令
        /// </summary>
        notfound = 7,
        /// <summary>
        /// 非预期 但是可以继续
        /// </summary>
        unexpected = 8,
        /// <summary>
        /// 已删除
        /// </summary>
        delete = 9
    }

    public enum NginxState
    {
        /// <summary>
        /// 等待检查
        /// </summary>
        waitcheck = 1,
        /// <summary>
        /// 路由格式校验通过
        /// </summary>
        checkok = 2,
        /// <summary>
        /// 遇到错误
        /// </summary>
        error = 3
    }

    /// <summary>
    /// 任务状态
    /// </summary>
    public enum OrderState
    {
        waitrun = 1,//待执行
        running = 2,//执行中
        success = 3,//执行成功
        finishe = 4,//执行完成，非预期结果，但是可以忽略
        error = 5//执行错误
    }

    /// <summary>
    /// 动作类型
    /// </summary>
    public enum ActionState
    {
        /// <summary>
        /// update to stop 手动停止
        /// </summary>
        sqlstop=1,
        /// <summary>
        /// update to run
        /// </summary>
        sqlrun=2,
        /// <summary>
        /// 监听状态变更停止
        /// </summary>
        sqlerror=3,
        /// <summary>
        /// docker run
        /// </summary>
        dockerrun=5,
        /// <summary>
        /// docker build
        /// </summary>
        dockerbuild=6,
        /// <summary>
        /// 推送镜像
        /// </summary>
        dockerpush=7,
        /// <summary>
        /// 
        /// </summary>
        dockerstop=8,
        /// <summary>
        /// 
        /// </summary>
        dockerrestart=9,
        /// <summary>
        /// 拉取镜像，如果不存在的话
        /// </summary>
        dockerpull=10,
        /// <summary>
        /// docker rm container
        /// </summary>
        dockerrm=11,
        /// <summary>
        /// 删除镜像 target image:tag 需要判断是否存在，然后删除，不存在则不操作
        /// </summary>
        dockerrmi=12,
        /// <summary>
        /// 等待
        /// </summary>
        waitdely=13,
        /// <summary>
        /// 等待启动
        /// </summary>
        dockerwaitrun=14,
        /// <summary>
        /// 自定义命令
        /// </summary>
        custom=15,
        /// <summary>
        /// 自定义命令，需要替换
        /// </summary>
        custommodel=16,
        /// <summary>
        /// 判断镜像是否存在
        /// </summary>
        imageexist=17,
        /// <summary>
        /// 读取daemon.json
        /// </summary>
        readdaemon=18,
        /// <summary>
        /// 上载daemon
        /// </summary>
        uploaddaemon=19,
        /// <summary>
        /// 上传仓储的证书
        /// </summary>
        uploadregistrycert=20,
        /// <summary>
        /// 上传普通文件 command:filepath target:filebody
        /// </summary>
        uploadfile=21,
        /// <summary>
        /// docker tag
        /// </summary>
        dockertag=22,
        /// <summary>
        /// docker rename
        /// </summary>
        dockerrename=23,
        /// <summary>
        /// docker login 到仓储
        /// </summary>
        dockerlogin=24,
        /// <summary>
        /// 指定Linux登陆到registry成功
        /// </summary>
        dockerlogined=25,
        /// <summary>
        /// 构建后升级 单个环境
        /// </summary>
        buildupdate=26,
        /// <summary>
        /// 构建后升级，多个环境
        /// </summary>
        buildupdates=27,
        /// <summary>
        /// 构建通知
        /// </summary>
        buildnotify=28,
        /// <summary>
        /// 检查端口是否被占用
        /// </summary>
        checkport=29,
        /// <summary>
        /// 更新openssl.cnf配置文件，先下载，然后替换host，给下一个任务备用 command是路径
        /// </summary>
        readsslcnf=30,
        /// <summary>
        /// 更新openssl.cnf需要上一个步骤的结果 command是路径
        /// </summary>
        uploadsslcnf=31,
        /// <summary>
        /// 保存registry的证书内容 target:{certname} {certpass}
        /// </summary>
        saveregistrycertbody=32,
        /// <summary>
        /// 判定文件是否存在
        /// </summary>
        fileexits=33,
        /// <summary>
        /// 仓储创建成功
        /// </summary>
        storebuildsuccess=34,
        /// <summary>
        /// 删除仓储中的镜像 target为version
        /// </summary>
        registryimageremove=35,
        /// <summary>
        /// 读取仓储中的镜像信息 target为version
        /// </summary>
        registryimageread=36,
        /// <summary>
        /// 更新daemon.json target为host
        /// </summary>
        daemonread=37,
        /// <summary>
        /// daemon更新后上传
        /// </summary>
        daemonupdate=38,
        /// <summary>
        /// 重新加载daemon并重启docker
        /// </summary>
        daemonreload=39,
        /// <summary>
        /// 测试拉取镜像，如果可以拉取则后续的动作不需要执行，拉取不了，则后续的需要执行
        /// </summary>
        daemonpulltry=40,
        /// <summary>
        /// 检查服务器的内存可用量
        /// </summary>
        checklinuxfree=41,
        /// <summary>
        /// 清理服务器换成
        /// </summary>
        linuxcacheclean=42
    }


}
