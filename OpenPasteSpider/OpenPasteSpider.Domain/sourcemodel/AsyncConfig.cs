﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.sourcemodel
{
    /// <summary>
    /// 上载配置 文件同步的文件配置
    /// </summary>
    [Comment("同步过滤")]
    public class AsyncConfig:Entity<int>
    {
        /// <summary>
        /// 服务ID
        /// </summary>
        [Comment("服务ID")]
        public int ServiceId { get; set; }

        /// <summary>
        /// 文件或者文件夹全名称 /开头
        /// </summary>
        [Comment("文件或者文件夹全名称")]
        public string FileName { get; set; }

        /// <summary>
        /// 是否文件夹
        /// </summary>
        [Comment("是否文件夹")]
        public bool IsDirectory { get; set; } = false;

        /// <summary>
        /// 忽略或者必须 否则为必须更替
        /// </summary>
        [Comment("忽略或者必须")]
        public bool Ignore { get; set; } = true;

        /// <summary>
        /// 启用
        /// </summary>
        [Comment("状态")]
        public bool IsEnable { get; set; } = true;



    }
}
