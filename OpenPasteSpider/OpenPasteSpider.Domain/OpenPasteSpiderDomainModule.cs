﻿using System;
using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace OpenPasteSpider
{
    [DependsOn(typeof(AbpDddDomainModule))]
    public class OpenPasteSpiderDomainModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            //Console.WriteLine("---OpenPasteSpiderDomainModule---");

        }
    }
}
