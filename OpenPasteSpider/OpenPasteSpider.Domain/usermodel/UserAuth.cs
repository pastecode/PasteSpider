﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.usermodel
{

    /// <summary>
    /// 用户授权
    /// </summary>
    [Comment("用户授权")]
    public class UserAuth : Entity<int>
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        [Comment("用户ID")]
        public int UserId { get; set; }

        ///// <summary>
        ///// 项目ID 是否需要这个？操作对象其实是服务
        ///// </summary>
        //[Comment("项目ID")]
        //public int ProjectId { get; set; }

        /// <summary>
        /// 服务ID
        /// </summary>
        [Comment("服务ID")]
        public int ServiceId { get; set; }

        /// <summary>
        /// 环境代码集 default,test,prod环境判断只发生在动作上
        /// </summary>
        [Comment("环境代码集")]
        [MaxLength(128)]
        public string ModelCodes { get; set; } = "default";

        /// <summary>
        /// 创建日期
        /// </summary>
        [Comment("创建日期")]
        public DateTime CreateDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 状态
        /// </summary>
        [Comment("状态")]
        public bool IsEnable { get; set; } = true;


        //具体授权由权限和这个表相互决定，root,root免判断
        //判断这个用户是否有这个服务的操作权限！
        ///// <summary>
        ///// 环境ID
        ///// </summary>
        //public int ModelId { get; set; }
    }
}
