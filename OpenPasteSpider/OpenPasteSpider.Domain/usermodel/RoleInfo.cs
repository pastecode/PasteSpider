﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.usermodel
{
    /// <summary>
    /// 权限列表
    /// </summary>
    [Comment("权限列表")]
    public class RoleInfo : Entity<int>
    {

        /// <summary>
        /// 权限模块
        /// </summary>
        [MaxLength(16)]
        [Comment("权限模块")]
        public string Model { get; set; }

        /// <summary>
        /// 此项的名称
        /// </summary>
        [MaxLength(16)]
        [Comment("此项的名称")]
        public string Name { get; set; }

        /// <summary>
        /// 权限值
        /// </summary>
        [MaxLength(64)]
        [Comment("权限值")]
        public string Role { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [MaxLength(64)]
        [Comment("描述")]
        public string Desc { get; set; }

        /// <summary>
        /// 父级ID
        /// </summary>
        [Comment("父级ID")]
        public int FatherId { get; set; } = 0;

        /// <summary>
        /// 权限类型 0权限 1菜单 2按钮
        /// </summary>
        [Comment("权限类型")]
        public int RoleType { get; set; } = 0;

        /// <summary>
        /// 层级簇
        /// </summary>
        [Comment("层级簇")]
        public string FatherStr { get; set; } = "";

        /// <summary>
        /// 状态
        /// </summary>
        [Comment("状态")]
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 排序
        /// </summary>
        [Comment("排序")]
        public int Sort { get; set; } = 100;

    }

}
