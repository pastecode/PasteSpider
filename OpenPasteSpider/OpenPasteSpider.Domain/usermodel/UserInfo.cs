﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.usermodel
{
    /// <summary>
    /// 用户信息
    /// </summary>
    [Comment("管理账号信息")]
    public class UserInfo : Entity<int>
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [MaxLength(32)]
        [Comment("用户名")]
        public string UserName { get; set; }

        /// <summary>
        /// 创建日期
        /// </summary>
        [Comment("创建日期")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [MaxLength(64)]
        [Comment("描述")]
        public string Desc { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [MaxLength(64)]
        [Comment("邮箱")]
        public string Email { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [MaxLength(64)]
        [Comment("密码")]
        public string PassWord { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Comment("状态")]
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 哪个角色？
        /// </summary>
        [MaxLength(16)]
        [Comment("哪个角色")]
        public string Grade { get; set; }
    }


}
