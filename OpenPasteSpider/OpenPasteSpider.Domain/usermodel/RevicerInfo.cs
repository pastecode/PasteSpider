﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.usermodel
{

    /// <summary>
    /// 接收者 钉钉或者飞书的webhook
    /// </summary>
    [Comment("消息推送接收者")]
    public class RevicerInfo : Entity<int>
    {
        /// <summary>
        /// 接收者名称
        /// </summary>
        [Comment("接收者名称")]
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 状态是否可用
        /// </summary>
        [Comment("状态是否可用")]
        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 推送地址 钉钉或者飞书的webhook
        /// </summary>
        [Comment("推送地址")]
        [MaxLength(256)]
        public string Url { get; set; }

        /// <summary>
        /// |all|  info warning error all 这个应该改版成等级 消息等级
        /// </summary>
        [Comment("接收消息类型")]
        public string Codes { get; set; }

        /// <summary>
        /// 推送次数
        /// </summary>
        [Comment("推送次数")]
        public int TotalSend { get; set; }

        /// <summary>
        /// 推送失败次数
        /// </summary>
        [Comment("推送失败次数")]
        public int TotalFailed { get; set; }

        /// <summary>
        /// 项目ID 0表示所有的，后续升级
        /// </summary>
        [Comment("项目ID")]
        public int ProjectId { get; set; }


    }


    ///// <summary>
    ///// 消息类型
    ///// </summary>
    //[Index(nameof(Code),IsUnique =true)]
    //public class MessageInfo:Entity<int>
    //{

    //    /// <summary>
    //    /// nodeoffline nodeexception taskfail taskexception
    //    /// </summary>
    //    [MaxLength(16)]
    //    [Comment("消息类型代码")]
    //    public string Code { get; set; }

    //    /// <summary>
    //    /// 介绍唯一key的计算方式
    //    /// </summary>
    //    [MaxLength(64)]
    //    [Comment("消息介绍")]
    //    public string Desc { get; set; }

    //    /// <summary>
    //    /// x(s) 发送频率大于1秒
    //    /// </summary>
    //    [Comment("消息发送最小频率")]
    //    public int SendRate { get; set; } = 300;

    //}

    ///// <summary>
    ///// 发送记录
    ///// </summary>
    //[Comment("消息推送记录")]
    //public class NoticeLog : Entity<int>
    //{

    //    /// <summary>
    //    /// 创建时间
    //    /// </summary>
    //    [Comment("发送时间")]
    //    public DateTime CreateDate { get; set; } = DateTimeOffset.Now.Date;

    //    /// <summary>
    //    /// 消息代码
    //    /// </summary>
    //    [MaxLength(16)]
    //    [Comment("消息代码")]
    //    public string Code { get; set; }

    //    /// <summary>
    //    /// 事件对象ID 忽略
    //    /// </summary>
    //    [Comment("对象ID")]
    //    public int ObjId { get; set; }

    //    /// <summary>
    //    /// 消息正文
    //    /// </summary>
    //    [Comment("消息内容")]
    //    public string Body { get; set; }

    //    /// <summary>
    //    /// 发生次数
    //    /// </summary>
    //    [Comment("发生频次")]
    //    public int Time { get; set; }

    //    /// <summary>
    //    /// 接收者
    //    /// </summary>
    //    [Comment("接收者")]
    //    public int RevicerId { get; set; }

    //    /// <summary>
    //    /// 推送结果
    //    /// </summary>
    //    [Comment("推送结果")]
    //    public bool Success { get; set; } = false;

    //}
}
