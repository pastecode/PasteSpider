﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.usermodel
{
    /// <summary>
    /// 角色分组
    /// </summary>
    [Index(nameof(Name), IsUnique = true)]
    [Comment("角色信息")]
    public class GradeInfo : Entity<int>
    {
        /// <summary>
        /// 角色分组名称
        /// </summary>
        [MaxLength(16)]
        [Comment("角色分组名称")]
        public string Name { get; set; }

        /// <summary>
        /// 分组描述
        /// </summary>
        [MaxLength(64)]
        [Comment("分组描述")]
        public string Desc { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [Comment("状态")]
        public bool IsEnable { get; set; } = true;

    }
}
