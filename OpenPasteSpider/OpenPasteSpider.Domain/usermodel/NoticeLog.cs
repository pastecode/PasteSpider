﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.usermodel
{
    /// <summary>
    /// 发送记录
    /// </summary>
    [Comment("消息推送记录")]
    public class NoticeLog : Entity<int>
    {

        /// <summary>
        /// 创建时间
        /// </summary>
        [Comment("发送时间")]
        public DateTime CreateDate { get; set; } = DateTimeOffset.Now.Date;

        /// <summary>
        /// 消息代码
        /// </summary>
        [MaxLength(16)]
        [Comment("消息代码")]
        public string Code { get; set; }

        /// <summary>
        /// 事件对象ID 忽略
        /// </summary>
        [Comment("对象ID")]
        public int ObjId { get; set; }

        /// <summary>
        /// 消息正文
        /// </summary>
        [Comment("消息内容")]
        public string Body { get; set; }

        /// <summary>
        /// 发生次数
        /// </summary>
        [Comment("发生频次")]
        public int Time { get; set; }

        /// <summary>
        /// 接收者
        /// </summary>
        [Comment("接收者")]
        public int RevicerId { get; set; }

        /// <summary>
        /// 推送结果
        /// </summary>
        [Comment("推送结果")]
        public bool Success { get; set; } = false;

    }
}
