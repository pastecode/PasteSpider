﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Domain.Entities;

namespace OpenPasteSpider.usermodel
{
    /// <summary>
    /// 角色绑定权限
    /// </summary>
    [Comment("角色绑定权限")]
    public class GradeRole : Entity<int>
    {
        /// <summary>
        /// 组别ID
        /// </summary>
        [Comment("组别ID")]
        public int GradeId { get; set; }

        /// <summary>
        /// 权限ID
        /// </summary>
        [Comment("权限ID")]
        public int RoleId { get; set; }
    }
}
