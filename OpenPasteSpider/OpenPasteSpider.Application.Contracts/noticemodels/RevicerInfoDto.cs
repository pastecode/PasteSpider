﻿using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.noticemodels
{
    /// <summary>
    /// 
    /// </summary>
    public class RevicerInfoDto : EntityDto<int>
    {

        ///<summary>
        ///接收者名称
        ///</summary>

        public string Name { get; set; }

        ///<summary>
        ///推送地址
        ///</summary>

        public string Url { get; set; }

        ///<summary>
        ///(.*)/taskexception|taskfail
        ///</summary>

        public string Codes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
    }
    /// <summary>
    /// 
    /// </summary>
    public class RevicerInfoAddDto
    {

        ///<summary>
        ///接收者名称
        ///</summary>

        public string Name { get; set; }

        ///<summary>
        ///推送地址
        ///</summary>

        public string Url { get; set; }

        ///<summary>
        ///(.*)/taskexception|taskfail
        ///</summary>
        public string Codes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
    }
    /// <summary>
    /// 
    /// </summary>
    public class RevicerInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///接收者名称
        ///</summary>
        public string Name { get; set; }

        ///<summary>
        ///推送地址
        ///</summary>
        public string Url { get; set; }

        ///<summary>
        ///(.*)/taskexception|taskfail
        ///</summary>
        public string Codes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
    }
    /// <summary>
    /// 
    /// </summary>
    public class RevicerInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///接收者名称
        ///</summary>

        public string Name { get; set; }

        ///<summary>
        ///推送地址
        ///</summary>

        public string Url { get; set; }

        ///<summary>
        ///(.*)/taskexception|taskfail
        ///</summary>

        public string Codes { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
    }
}
