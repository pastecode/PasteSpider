﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenPasteSpider
{

    /// <summary>
    /// 登录后的返回值
    /// </summary>
    public class LoginOutputModel {

        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; } = 0;

        /// <summary>
        /// 用户昵称，用户名
        /// </summary>
        public string UserName { get; set; } = "";


        /// <summary>
        /// 密钥
        /// </summary>
        public string Token { get; set; } = "";

        /// <summary>
        /// 权限字符串
        /// </summary>
        public string RoleString { get; set; } = "";

        /// <summary>
        /// 过期时长
        /// </summary>
        public int TimeExpire { get; set; } = 3600;
    }

    /// <summary>
    /// 服务器内存情况
    /// </summary>
    public class ModelLinuxMemoryInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public double Total { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Used { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Free { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Shared { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Buff { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public double Available { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class ActionBack {

        public int Code { get; set; } = 200;

        public string Message { get; set; } = "操作成功!";
    }
}
