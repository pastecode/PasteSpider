﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.projectmodel
{

    public class ServiceInfoAddDto
    {

        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 直接目录，一般用于前端静态,需要在工作目录下
        /// </summary>
        [MaxLength(128)]
        public string DirectPath { get; set; }

        ///<summary>
        ///服务的详细描述
        ///</summary>
        [MaxLength(128)]
        public string Desc { get; set; }

        ///<summary>
        ///是否可用
        ///</summary>

        public bool IsEnable { get; set; } = true;

        /// <summary>
        /// 0静态 1源码 2发布 3镜像模式
        /// </summary>
        public int FileModel { get; set; } = 0;

        /// <summary>
        /// 镜像名称 当文件类型为镜像模式的时候使用这个xxx:xxx
        /// </summary>
        [MaxLength(256)]
        public string DirectImage { get; set; } = "";


        ///<summary>
        ///当前镜像版本
        ///</summary>

        public int Version { get; set; }

        ///<summary>
        ///配置变动推送地址
        ///</summary>
        [MaxLength(128)]
        public string ConfigNotify { get; set; }

        ///<summary>
        ///监听端口
        ///</summary>
        [MaxLength(128)]
        public string ListenPorts { get; set; }

        ///<summary>
        ///是否需要端口映射
        ///</summary>

        public bool NeedMapping { get; set; }

        ///<summary>
        ///升级前置命令
        ///</summary>

        public string BeforeCommands { get; set; }

        ///<summary>
        ///是否自动重启
        ///</summary>

        public bool AutoRestart { get; set; }

        ///<summary>
        ///升级后置命令
        ///</summary>

        public string AfterCommands { get; set; }

        ///<summary>
        ///升级拆分至多几次
        ///</summary>

        public int UpdateSplits { get; set; }

        ///<summary>
        ///保存的版本号！
        ///</summary>
        [DefaultValue(5)]
        public int SaveNumber { get; set; }

        ///<summary>
        ///其他参数
        ///</summary>

        public string OtherArgs { get; set; }

        ///<summary>
        ///环境参数
        ///</summary>

        public string EnvironmentArgs { get; set; }

        ///<summary>
        ///命名空间
        ///</summary>

        public int ProjectId { get; set; }

        /// <summary>
        /// 需要同步开通的环境信息
        /// </summary>
        public string[] models { get; set; }
    }

    /// <summary>
    /// 服务的简短信息 Id Code Name
    /// </summary>
    public class ServiceShortModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

    }


    public class ServiceInfoDto : EntityDto<int>
    {

        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }
        
        /// <summary>
        /// 直接目录，一般用于前端静态,需要在工作目录下/开头非/结尾的文件夹路径，是app内的路径
        /// </summary>
        [MaxLength(128)]
        public string DirectPath { get; set; }

        ///<summary>
        ///服务的详细描述
        ///</summary>
        [MaxLength(128)]
        public string Desc { get; set; }

        ///<summary>
        ///是否可用
        ///</summary>

        public bool IsEnable { get; set; }

        /// <summary>
        /// 0静态 1源码 2发布 3镜像模式
        /// </summary>
        public int FileModel { get; set; } = 0;

        /// <summary>
        /// 镜像名称 当文件类型为镜像模式的时候使用这个xxx:xxx
        /// </summary>
        [MaxLength(256)]
        public string DirectImage { get; set; } = "";

        ///<summary>
        ///镜像名称前缀
        ///</summary>
        [MaxLength(32)]
        public string ImagePrefix { get; set; }

        ///<summary>
        ///当前镜像版本
        ///</summary>
        public int Version { get; set; }

        ///<summary>
        ///配置变动推送地址
        ///</summary>
        [MaxLength(128)]
        public string ConfigNotify { get; set; }

        ///<summary>
        ///监听端口
        ///</summary>
        [MaxLength(128)]
        public string ListenPorts { get; set; }

        ///<summary>
        ///是否需要端口映射
        ///</summary>

        public bool NeedMapping { get; set; }

        ///<summary>
        ///升级前置命令
        ///</summary>

        public string BeforeCommands { get; set; }

        ///<summary>
        ///是否自动重启
        ///</summary>

        public bool AutoRestart { get; set; }

        ///<summary>
        ///升级后置命令
        ///</summary>

        public string AfterCommands { get; set; }

        ///<summary>
        ///升级拆分至多几次
        ///</summary>

        public int UpdateSplits { get; set; }

        ///<summary>
        ///保存的版本号！
        ///</summary>
        [DefaultValue(5)]
        public int SaveNumber { get; set; }

        ///<summary>
        ///其他参数
        ///</summary>

        public string OtherArgs { get; set; }

        ///<summary>
        ///环境参数
        ///</summary>

        public string EnvironmentArgs { get; set; }

        ///<summary>
        ///命名空间
        ///</summary>

        public ProjectInfoDto Project { get; set; }
    }

    public class ServiceInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 直接目录，一般用于前端静态,需要在工作目录下
        /// </summary>
        [MaxLength(128)]
        public string DirectPath { get; set; }

        ///<summary>
        ///服务的详细描述
        ///</summary>
        [MaxLength(128)]
        public string Desc { get; set; }

        ///<summary>
        ///是否可用
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///镜像名称前缀
        ///</summary>
        [MaxLength(32)]
        [Required]
        public string ImagePrefix { get; set; }
        /// <summary>
        /// 0静态 1源码 2发布 3镜像模式
        /// </summary>
        public int FileModel { get; set; } = 0;

        /// <summary>
        /// 镜像名称 当文件类型为镜像模式的时候使用这个xxx:xxx
        /// </summary>
        [MaxLength(256)]
        public string DirectImage { get; set; } = "";
        ///<summary>
        ///当前镜像版本
        ///</summary>

        public int Version { get; set; }

        ///<summary>
        ///配置变动推送地址
        ///</summary>
        [MaxLength(128)]
        public string ConfigNotify { get; set; }

        ///<summary>
        ///监听端口
        ///</summary>
        [MaxLength(128)]
        public string ListenPorts { get; set; }

        ///<summary>
        ///是否需要端口映射
        ///</summary>

        public bool NeedMapping { get; set; }

        ///<summary>
        ///升级前置命令
        ///</summary>

        public string BeforeCommands { get; set; }

        ///<summary>
        ///是否自动重启
        ///</summary>

        public bool AutoRestart { get; set; }

        ///<summary>
        ///升级后置命令
        ///</summary>

        public string AfterCommands { get; set; }

        ///<summary>
        ///升级拆分至多几次
        ///</summary>

        public int UpdateSplits { get; set; }

        ///<summary>
        ///保存的版本号！
        ///</summary>
        [DefaultValue(5)]
        public int SaveNumber { get; set; }

        ///<summary>
        ///其他参数
        ///</summary>

        public string OtherArgs { get; set; }

        ///<summary>
        ///环境参数
        ///</summary>

        public string EnvironmentArgs { get; set; }

        ///<summary>
        ///命名空间
        ///</summary>

        public ProjectInfoDto Project { get; set; }
    }

    public class ServiceInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 直接目录，一般用于前端静态,需要在工作目录下
        /// </summary>
        [MaxLength(128)]
        public string DirectPath { get; set; }

        ///<summary>
        ///服务的详细描述
        ///</summary>
        [MaxLength(128)]
        public string Desc { get; set; }

        ///<summary>
        ///是否可用
        ///</summary>

        public bool IsEnable { get; set; }

        /// <summary>
        /// 0静态 1源码 2发布 3镜像模式
        /// </summary>
        public int FileModel { get; set; } = 0;

        /// <summary>
        /// 镜像名称 当文件类型为镜像模式的时候使用这个xxx:xxx
        /// </summary>
        [MaxLength(256)]
        public string DirectImage { get; set; } = "";
        ///<summary>
        ///当前镜像版本
        ///</summary>

        public int Version { get; set; }

        ///<summary>
        ///配置变动推送地址
        ///</summary>
        [MaxLength(128)]
        public string ConfigNotify { get; set; }

        ///<summary>
        ///监听端口
        ///</summary>
        [MaxLength(128)]
        public string ListenPorts { get; set; }

        ///<summary>
        ///是否需要端口映射
        ///</summary>

        public bool NeedMapping { get; set; }

        ///<summary>
        ///升级前置命令
        ///</summary>

        public string BeforeCommands { get; set; }

        ///<summary>
        ///是否自动重启
        ///</summary>

        public bool AutoRestart { get; set; }

        ///<summary>
        ///升级后置命令
        ///</summary>

        public string AfterCommands { get; set; }

        ///<summary>
        ///升级拆分至多几次
        ///</summary>

        public int UpdateSplits { get; set; }

        ///<summary>
        ///保存的版本号！
        ///</summary>
        [DefaultValue(5)]
        public int SaveNumber { get; set; }

        ///<summary>
        ///其他参数
        ///</summary>

        public string OtherArgs { get; set; }

        ///<summary>
        ///环境参数
        ///</summary>

        public string EnvironmentArgs { get; set; }

        ///<summary>
        ///命名空间
        ///</summary>

        public int ProjectId { get; set; }
    }
}
