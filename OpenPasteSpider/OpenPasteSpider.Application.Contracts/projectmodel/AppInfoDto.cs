﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.projectmodel
{
    public class AppInfoDto : EntityDto<int>
    {

        ///<summary>
        ///容器名称
        ///</summary>
        [DefaultValue("")]
        public string Name { get; set; }

        ///<summary>
        ///简略ID
        ///</summary>
        [MaxLength(12)]
        public string AppID { get; set; }

        ///<summary>
        ///容器IP
        ///</summary>
        [MaxLength(16)]
        public string Address { get; set; }

        ///<summary>
        ///映射端口
        ///</summary>
        [MaxLength(128)]
        public string OutPort { get; set; }

        ///<summary>
        ///运行状态
        ///</summary>

        public RunState StateCode { get; set; }

        ///<summary>
        ///当前运行版本
        ///</summary>

        public int Version { get; set; }

        ///<summary>
        ///率属于哪个模型
        ///</summary>

        public ModelInfoDto Model { get; set; }

        ///<summary>
        ///运行在哪台服务器
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///哪一个项目的
        ///</summary>

        public int ProjectId { get; set; }
    }

    public class AppInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///容器名称
        ///</summary>
        [DefaultValue("")]
        public string Name { get; set; }

        ///<summary>
        ///简略ID
        ///</summary>
        [MaxLength(12)]
        public string AppID { get; set; }

        ///<summary>
        ///容器IP
        ///</summary>
        [MaxLength(16)]
        public string Address { get; set; }

        ///<summary>
        ///映射端口
        ///</summary>
        [MaxLength(128)]
        public string OutPort { get; set; }

        ///<summary>
        ///运行状态
        ///</summary>

        public RunState StateCode { get; set; }

        ///<summary>
        ///当前运行版本
        ///</summary>

        public int Version { get; set; }

        ///<summary>
        ///率属于哪个模型
        ///</summary>

        public ModelInfoDto Model { get; set; }

        ///<summary>
        ///运行在哪台服务器
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///哪一个项目的
        ///</summary>

        public int ProjectId { get; set; }
    }
}
