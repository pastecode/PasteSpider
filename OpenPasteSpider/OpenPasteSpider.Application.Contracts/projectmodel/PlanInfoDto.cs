﻿using System;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.projectmodel
{
    public class PlanInfoDto : EntityDto<int>
    {

        ///<summary>
        ///任务类型
        ///</summary>

        public OrderModel OrderModel { get; set; }

        ///<summary>
        ///待执行
        ///</summary>

        public RunState State { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>

        public DateTime CreateDate { get; set; }

        ///<summary>
        ///执行时长
        ///</summary>

        public int Durtion { get; set; }

        ///<summary>
        ///执行服务器
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///目标服务ID
        ///</summary>

        public int ServiceId { get; set; }

        ///<summary>
        ///目标模板ID
        ///</summary>

        public int ModelId { get; set; }

        ///<summary>
        ///目标Container
        ///</summary>

        public int AppId { get; set; }

        ///<summary>
        ///路由文件ID
        ///</summary>

        public int NginxId { get; set; }

        ///<summary>
        ///目标仓库
        ///</summary>

        public int StoreId { get; set; }

        ///<summary>
        ///目标项目
        ///</summary>

        public int ProjectId { get; set; }

        ///<summary>
        ///目标版本号
        ///</summary>

        public int Version { get; set; }
    }
    public class PlanInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///任务类型
        ///</summary>

        public OrderModel OrderModel { get; set; }

        ///<summary>
        ///待执行
        ///</summary>

        public RunState State { get; set; }

        ///<summary>
        ///创建时间
        ///</summary>

        public DateTime CreateDate { get; set; }

        ///<summary>
        ///执行时长
        ///</summary>

        public int Durtion { get; set; }

        ///<summary>
        ///执行服务器
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///目标服务ID
        ///</summary>

        public int ServiceId { get; set; }

        ///<summary>
        ///目标模板ID
        ///</summary>

        public int ModelId { get; set; }

        ///<summary>
        ///目标Container
        ///</summary>

        public int AppId { get; set; }

        ///<summary>
        ///路由文件ID
        ///</summary>

        public int NginxId { get; set; }

        ///<summary>
        ///目标仓库
        ///</summary>

        public int StoreId { get; set; }

        ///<summary>
        ///目标项目
        ///</summary>

        public int ProjectId { get; set; }

        ///<summary>
        ///目标版本号
        ///</summary>

        public int Version { get; set; }

        /// <summary>
        /// 扩展 服务名称
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// 仓储名称
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 服务器名称
        /// </summary>
        public string LinuxName { get; set; }

        /// <summary>
        /// 模式环境
        /// </summary>
        public string ModelCode { get; set; }
    }
}
