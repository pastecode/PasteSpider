﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.projectmodel
{

    public class ProjectInfoAddDto
    {

        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 变更后通知地址 可以自己在地址中附带token 会推送路由的消息体过去
        /// </summary>
        [MaxLength(256)]
        public string NotifyUrl { get; set; } = "";

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(128)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

    }
    public class ProjectInfoDto : EntityDto<int>
    {

        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 变更后通知地址 可以自己在地址中附带token 会推送路由的消息体过去
        /// </summary>
        [MaxLength(256)]
        public string NotifyUrl { get; set; } = "";
        ///// <summary>
        ///// 
        ///// </summary>
        //public string Host { get; set; } = "";
        ///// <summary>
        ///// 
        ///// </summary>
        //public string WebPath { get; set; } = "";

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(128)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///服务个数
        ///</summary>
        public int TotalService { get; set; }
    }
    public class ProjectInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }
        /// <summary>
        /// 变更后通知地址 可以自己在地址中附带token 会推送路由的消息体过去
        /// </summary>
        [MaxLength(256)]
        public string NotifyUrl { get; set; } = "";
        ///// <summary>
        ///// 
        ///// </summary>
        //public string Host { get; set; } = "";

        ///// <summary>
        ///// 
        ///// </summary>
        //public string WebPath { get; set; } = "";

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(128)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///服务个数
        ///</summary>
        public int TotalService { get; set; }
    }
    public class ProjectInfoUpdateDto : EntityDto<int>
    {
        /// <summary>
        /// 变更后通知地址 可以自己在地址中附带token 会推送路由的消息体过去
        /// </summary>
        [MaxLength(256)]
        public string NotifyUrl { get; set; } = "";
        ///<summary>
        ///项目代码
        ///</summary>
        [MaxLength(16)]
        public string Code { get; set; }

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //public string Host { get; set; } = "";

        ///// <summary>
        ///// 
        ///// </summary>
        //public string WebPath { get; set; } = "";

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(128)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

    }
}
