﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.projectmodel
{
    public class LinuxInfoAddDto
    {

        ///<summary>
        ///服务器名称
        ///</summary>
        [MaxLength(16)]
        [Required(ErrorMessage = "用名称便于识别服务器是哪台，不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 机器名称 用于Host指定，可以从后台读取
        /// </summary>
        [MaxLength(64)]
        public string LinuxName { get; set; } = "";


        ///<summary>
        ///外网IP信息
        ///</summary>
        [MaxLength(16)]
        public string RemoteIP { get; set; }

        ///<summary>
        ///局域网的IP
        ///</summary>
        [Required]
        [MaxLength(16)]
        public string LocalIP { get; set; }

        ///<summary>
        ///使用命令检查端口是否被占用了
        ///</summary>
        [MaxLength(128)]
        public string OpenPorts { get; set; }

        /// <summary>
        /// 是否是构建服务器
        /// </summary>
        public bool BoolBuild { get; set; } = false;

        /// <summary>
        /// 构建访问地址 需要外部可以访问
        /// </summary>
        public string BuildHost { get; set; } = "";

        ///<summary>
        ///是否接受扩容新任务
        ///</summary>
        public bool AcceptExpand { get; set; }

        ///<summary>
        ///可用?
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///工作目录
        ///</summary>
        [MaxLength(32)]
        public string WorkDir { get; set; }

        ///<summary>
        ///NGinx的配置文件
        ///</summary>
        [MaxLength(128)]
        public string NginxDir { get; set; }

        ///<summary>
        ///SSHIP地址
        ///</summary>
        [MaxLength(16)]
        public string SSHAddress { get; set; }

        ///<summary>
        ///远程端口
        ///</summary>
        public int SSHPort { get; set; }

        ///<summary>
        ///远程账号
        ///</summary>
        [MaxLength(24)]
        public string SSHUser { get; set; }

        ///<summary>
        ///远程密码
        ///</summary>
        [MaxLength(64)]
        public string SSHPass { get; set; }

        ///<summary>
        ///远程登录证书内容
        ///</summary>
        public string SSHCertBody { get; set; }

        ///<summary>
        ///证书密码
        ///</summary>
        [MaxLength(32)]
        public string SSHCertPass { get; set; }

        ///<summary>
        ///系统版本
        ///</summary>
        [MaxLength(16)]
        public string Unix { get; set; }
        
        /// <summary>
        /// 容器类型 docker podman
        /// </summary>
        [MaxLength(16)]
        public string Tool { get; set; } = "docker";

        /// <summary>
        /// 链接密钥 256
        /// </summary>
        public string LinkToken { get; set; } = "";

        /// <summary>
        /// 空余内存MB
        /// </summary>
        public int FreeMemory { get; set; } = 500;

    }
    public class LinuxInfoDto : EntityDto<int>
    {

        ///<summary>
        ///服务器名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 机器名称 用于Host指定
        /// </summary>
        [MaxLength(64)]
        public string LinuxName { get; set; } = "";

        /// <summary>
        /// 组别名称 不需要管理端指定，由后台设定
        /// </summary>
        [MaxLength(8)]
        public string SlaveName { get; set; } = "default";

        ///<summary>
        ///外网IP信息
        ///</summary>
        [MaxLength(16)]
        public string RemoteIP { get; set; }

        ///<summary>
        ///局域网的IP
        ///</summary>
        [Required]
        [MaxLength(16)]
        public string LocalIP { get; set; }

        /// <summary>
        /// 是否是构建服务器
        /// </summary>
        public bool BoolBuild { get; set; } = false;

        /// <summary>
        /// 构建访问地址 需要外部可以访问
        /// </summary>
        public string BuildHost { get; set; } = "";

        ///<summary>
        ///使用命令检查端口是否被占用了
        ///</summary>
        [MaxLength(128)]
        public string OpenPorts { get; set; }

        ///<summary>
        ///是否接受扩容新任务
        ///</summary>

        public bool AcceptExpand { get; set; }

        ///<summary>
        ///可用?
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///工作目录
        ///</summary>
        [MaxLength(32)]
        public string WorkDir { get; set; }

        ///<summary>
        ///NGinx的配置文件
        ///</summary>
        [MaxLength(128)]
        public string NginxDir { get; set; }

        ///<summary>
        ///SSHIP地址
        ///</summary>
        [MaxLength(16)]
        public string SSHAddress { get; set; }

        ///<summary>
        ///远程端口
        ///</summary>

        public int SSHPort { get; set; }

        ///<summary>
        ///远程账号
        ///</summary>
        [MaxLength(24)]
        public string SSHUser { get; set; }

        ///<summary>
        ///远程密码
        ///</summary>
        [MaxLength(64)]
        public string SSHPass { get; set; }

        /////<summary>
        /////远程登录证书内容
        /////</summary>
        //public string SSHCertBody { get; set; }

        /////<summary>
        /////证书密码
        /////</summary>
        //[MaxLength(32)]
        //public string SSHCertPass { get; set; }
        /// <summary>
        /// 容器类型 docker podman
        /// </summary>
        [MaxLength(16)]
        public string Tool { get; set; } = "docker";
        ///<summary>
        ///系统版本
        ///</summary>
        [MaxLength(16)]
        public string Unix { get; set; }

        /// <summary>
        /// 链接密钥
        /// </summary>
        [MaxLength(256)]
        public string LinkToken { get; set; } = "";

        /// <summary>
        /// 链接码
        /// </summary>
        [Comment("链接专用码")]
        public string LinkCode { get; set; } = "";

        /// <summary>
        /// 空余内存MB
        /// </summary>
        public int FreeMemory { get; set; } = 500;
    }
    /// <summary>
    /// 
    /// </summary>
    public class LinuxInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///服务器名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 机器名称 用于Host指定
        /// </summary>
        [MaxLength(64)]
        public string LinuxName { get; set; } = "";

        /// <summary>
        /// 组别名称 不需要管理端指定，由后台设定
        /// </summary>
        [MaxLength(8)]
        public string SlaveName { get; set; } = "default";

        ///<summary>
        ///外网IP信息
        ///</summary>
        [MaxLength(16)]
        public string RemoteIP { get; set; }

        ///<summary>
        ///局域网的IP
        ///</summary>
        [Required]
        [MaxLength(16)]
        public string LocalIP { get; set; }

        /// <summary>
        /// 是否是构建服务器
        /// </summary>
        public bool BoolBuild { get; set; } = false;

        /// <summary>
        /// 构建访问地址 需要外部可以访问
        /// </summary>
        public string BuildHost { get; set; } = "";

        ///<summary>
        ///使用命令检查端口是否被占用了
        ///</summary>
        [MaxLength(128)]
        public string OpenPorts { get; set; }

        ///<summary>
        ///是否接受扩容新任务
        ///</summary>
        public bool AcceptExpand { get; set; }

        ///<summary>
        ///可用?
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///工作目录
        ///</summary>
        [MaxLength(32)]
        public string WorkDir { get; set; }

        ///<summary>
        ///NGinx的配置文件
        ///</summary>
        [MaxLength(128)]
        public string NginxDir { get; set; }

        ///<summary>
        ///SSHIP地址
        ///</summary>
        [MaxLength(16)]
        public string SSHAddress { get; set; }

        ///<summary>
        ///远程端口
        ///</summary>
        public int SSHPort { get; set; }

        ///<summary>
        ///远程账号
        ///</summary>
        [MaxLength(24)]
        public string SSHUser { get; set; }


        ///<summary>
        ///系统版本
        ///</summary>
        [MaxLength(16)]
        public string Unix { get; set; }

        /// <summary>
        /// 扩展 绑定store的状态
        /// </summary>
        public int BindStoreState { get; set; }

        /// <summary>
        /// 空余内存MB
        /// </summary>
        public int FreeMemory { get; set; } = 500;
        /// <summary>
        /// 容器类型 docker podman
        /// </summary>
        [MaxLength(16)]
        public string Tool { get; set; } = "docker";
    }

    /// <summary>
    /// 
    /// </summary>
    public class LinuxInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///服务器名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 机器名称 用于Host指定
        /// </summary>
        [MaxLength(64)]
        public string LinuxName { get; set; } = "";

        ///<summary>
        ///外网IP信息
        ///</summary>
        [MaxLength(16)]
        public string RemoteIP { get; set; }

        ///<summary>
        ///局域网的IP
        ///</summary>
        [Required]
        [MaxLength(16)]
        public string LocalIP { get; set; }

        ///<summary>
        ///使用命令检查端口是否被占用了
        ///</summary>
        [MaxLength(128)]
        public string OpenPorts { get; set; }


        /// <summary>
        /// 是否是构建服务器
        /// </summary>
        public bool BoolBuild { get; set; } = false;

        /// <summary>
        /// 构建访问地址 需要外部可以访问
        /// </summary>
        public string BuildHost { get; set; } = "";

        ///<summary>
        ///是否接受扩容新任务
        ///</summary>
        public bool AcceptExpand { get; set; }

        ///<summary>
        ///可用?
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///工作目录
        ///</summary>
        [MaxLength(32)]
        public string WorkDir { get; set; }

        ///<summary>
        ///NGinx的配置文件
        ///</summary>
        [MaxLength(128)]
        public string NginxDir { get; set; }

        ///<summary>
        ///SSHIP地址
        ///</summary>
        [MaxLength(16)]
        public string SSHAddress { get; set; }

        ///<summary>
        ///远程端口
        ///</summary>
        public int SSHPort { get; set; }

        ///<summary>
        ///远程账号
        ///</summary>
        [MaxLength(24)]
        public string SSHUser { get; set; }

        ///<summary>
        ///远程密码
        ///</summary>
        [MaxLength(64)]
        public string SSHPass { get; set; }

        ///<summary>
        ///远程登录证书内容
        ///</summary>
        public string SSHCertBody { get; set; }

        ///<summary>
        ///证书密码
        ///</summary>
        [MaxLength(32)]
        public string SSHCertPass { get; set; }

        ///<summary>
        ///系统版本
        ///</summary>
        [MaxLength(16)]
        public string Unix { get; set; }

        /// <summary>
        /// 链接密钥
        /// </summary>
        [MaxLength(256)]
        public string LinkToken { get; set; } = "";

        /// <summary>
        /// 空余内存MB
        /// </summary>
        public int FreeMemory { get; set; } = 500;
        /// <summary>
        /// 容器类型 docker podman
        /// </summary>
        [MaxLength(16)]
        public string Tool { get; set; } = "docker";
    }
}
