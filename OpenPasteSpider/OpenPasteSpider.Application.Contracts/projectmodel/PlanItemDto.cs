﻿using System;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.projectmodel
{
    public class PlanItemDto : EntityDto<int>
    {

        ///<summary>
        ///主任务ID
        ///</summary>

        public int PlanInfoId { get; set; }

        ///<summary>
        ///哪一个动作
        ///</summary>

        public ActionState ActionType { get; set; }

        ///<summary>
        ///作废
        ///</summary>

        public DateTime CreateDate { get; set; }

        ///<summary>
        ///执行状态
        ///</summary>

        public RunState ExecState { get; set; }

        ///<summary>
        ///执行命令
        ///</summary>

        public string Command { get; set; }

        ///<summary>
        ///子命令的顺序
        ///</summary>

        public int RunIndex { get; set; }

        ///<summary>
        ///执行的反馈
        ///</summary>

        public string ActionResult { get; set; }

        ///<summary>
        ///目标值
        ///</summary>

        public string Target { get; set; }

        ///<summary>
        ///是否严格模式
        ///</summary>

        public bool StrictMode { get; set; }

        ///<summary>
        ///耗时
        ///</summary>

        public int Durtion { get; set; }

        ///<summary>
        ///退出代码
        ///</summary>

        public int ExitCode { get; set; }

        ///<summary>
        ///ContainerId
        ///</summary>

        public int AppId { get; set; }
    }
    public class PlanItemListDto : EntityDto<int>
    {

        ///<summary>
        ///主任务ID
        ///</summary>

        public int PlanInfoId { get; set; }

        ///<summary>
        ///哪一个动作
        ///</summary>

        public ActionState ActionType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ActionStr { get { return ActionType.ToString(); } }

        ///<summary>
        ///作废
        ///</summary>

        public DateTime CreateDate { get; set; }

        ///<summary>
        ///执行状态
        ///</summary>

        public RunState ExecState { get; set; }

        ///<summary>
        ///执行命令
        ///</summary>

        public string Command { get; set; }

        ///<summary>
        ///子命令的顺序
        ///</summary>

        public int RunIndex { get; set; }


        ///<summary>
        ///目标值
        ///</summary>

        public string Target { get; set; }

        ///<summary>
        ///是否严格模式
        ///</summary>

        public bool StrictMode { get; set; }

        ///<summary>
        ///耗时
        ///</summary>

        public int Durtion { get; set; }

        ///<summary>
        ///退出代码
        ///</summary>

        public int ExitCode { get; set; }

        ///<summary>
        ///ContainerId
        ///</summary>

        public int AppId { get; set; }
    }
}
