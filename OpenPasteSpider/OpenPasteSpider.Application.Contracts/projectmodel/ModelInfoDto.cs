﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.projectmodel
{
    /// <summary>
    /// 
    /// </summary>
    public class ModelInfoDto : EntityDto<int>
    {

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(8)]
        public string Code { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(32)]
        public string Desc { get; set; }

        /////<summary>
        /////构建失败次数
        /////</summary>

        //public int NumFailedBuild { get; set; }

        ///<summary>
        ///伸缩次数
        ///</summary>

        public int NumResize { get; set; }

        ///<summary>
        ///伸缩失败次数
        ///</summary>

        public int NumFailedResize { get; set; }

        ///<summary>
        ///升级次数
        ///</summary>

        public int NumUpdate { get; set; }

        ///<summary>
        ///升级失败次数
        ///</summary>

        public int NumFailedUpdate { get; set; }

        ///<summary>
        ///运行app数
        ///</summary>

        public int TotalAppNum { get; set; }

        ///<summary>
        ///哪一个服务
        ///</summary>

        public ServiceInfoDto Service { get; set; }

        ///<summary>
        ///项目ID
        ///</summary>
        public int ProjectId { get; set; }

        ///// <summary>
        ///// 状态
        ///// </summary>
        //public bool IsEnable { get; set; } = true;
    }

    /// <summary>
    /// 
    /// </summary>
    public class ModelInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(8)]
        public string Code { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(32)]
        public string Desc { get; set; }

        /////<summary>
        /////构建失败次数
        /////</summary>

        //public int NumFailedBuild { get; set; }

        /////<summary>
        /////伸缩次数
        /////</summary>

        //public int NumResize { get; set; }

        /////<summary>
        /////伸缩失败次数
        /////</summary>

        //public int NumFailedResize { get; set; }

        /////<summary>
        /////升级次数
        /////</summary>

        //public int NumUpdate { get; set; }

        /////<summary>
        /////升级失败次数
        /////</summary>

        //public int NumFailedUpdate { get; set; }

        /////<summary>
        /////运行app数
        /////</summary>

        //public int TotalAppNum { get; set; }

        ///<summary>
        ///哪一个服务
        ///</summary>

        public int ServiceId { get; set; }

        ///// <summary>
        ///// 状态
        ///// </summary>
        //public bool IsEnable { get; set; } = true;

        /////<summary>
        /////项目ID
        /////</summary>

        //public int ProjectId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ModelInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(8)]
        public string Code { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(32)]
        public string Desc { get; set; }

        /////<summary>
        /////构建失败次数
        /////</summary>

        //public int NumFailedBuild { get; set; }

        ///<summary>
        ///伸缩次数
        ///</summary>
        public int NumResize { get; set; }

        ///<summary>
        ///伸缩失败次数
        ///</summary>
        public int NumFailedResize { get; set; }

        ///<summary>
        ///升级次数
        ///</summary>
        public int NumUpdate { get; set; }

        ///<summary>
        ///升级失败次数
        ///</summary>
        public int NumFailedUpdate { get; set; }

        ///<summary>
        ///运行app数
        ///</summary>
        public int TotalAppNum { get; set; }

        ///<summary>
        ///哪一个服务
        ///</summary>
        public ServiceInfoDto Service { get; set; }

        ///<summary>
        ///项目ID
        ///</summary>
        public int ProjectId { get; set; }

        ///// <summary>
        ///// 状态
        ///// </summary>
        //public bool IsEnable { get; set; } = true;
    }

    /// <summary>
    /// 
    /// </summary>
    public class ModelInfoAddDto
    {

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(8)]
        public string Code { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(32)]
        public string Desc { get; set; }

        /////<summary>
        /////构建失败次数
        /////</summary>

        //public int NumFailedBuild { get; set; }

        /////<summary>
        /////伸缩次数
        /////</summary>

        //public int NumResize { get; set; }

        /////<summary>
        /////伸缩失败次数
        /////</summary>

        //public int NumFailedResize { get; set; }

        /////<summary>
        /////升级次数
        /////</summary>

        //public int NumUpdate { get; set; }

        /////<summary>
        /////升级失败次数
        /////</summary>

        //public int NumFailedUpdate { get; set; }

        /////<summary>
        /////运行app数
        /////</summary>

        //public int TotalAppNum { get; set; }

        ///<summary>
        ///哪一个服务
        ///</summary>

        public int ServiceId { get; set; }

        /// <summary>
        /// 绑定哪些服务器
        /// </summary>
        public int[] linuxs { get; set; }

        ///// <summary>
        ///// 状态
        ///// </summary>
        //public bool IsEnable { get; set; } = true;

        /////<summary>
        /////项目ID
        /////</summary>

        //public int ProjectId { get; set; }
    }
}
