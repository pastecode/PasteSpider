﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenPasteSpider
{


    /// <summary>
    /// 上传文件
    /// </summary>
    public class DropFileItem
    {
        /// <summary>
        /// 完整路径 /开头
        /// </summary>
        public string fullPath { get; set; }

        /// <summary>
        /// 最后修改时间 毫秒时间戳
        /// </summary>
        public long lastModified { get; set; }

        /// <summary>
        /// 文件名带后缀
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// UtcTimeStamp
        /// </summary>
        public long utctime { get; set; }
        /// <summary>
        /// 文件的md5
        /// </summary>
        public string md5 { get; set; }
    }

    /// <summary>
    /// Linux机器的CPU和内存情况
    /// </summary>
    public class LinuxState
    {
        /// <summary>
        /// 哪一台服务器的
        /// </summary>
        public int LinuxId { get; set; }

        /// <summary>
        /// 数据时间
        /// </summary>
        public DateTime DataDate { get; set; }

        /// <summary>
        /// CPU当前使用量或平均值
        /// </summary>
        public decimal CPUUsed { get; set; }

        /// <summary>
        /// 1分钟CPU平均值
        /// </summary>
        public decimal CPU1 { get; set; }

        /// <summary>
        /// 5分钟CPU平均值
        /// </summary>
        public decimal CPU2 { get; set; }

        /// <summary>
        /// 15分钟CPU平均值
        /// </summary>
        public decimal CPU3 { get; set; }

        /// <summary>
        /// 使用内存MB或平均值
        /// </summary>
        public decimal MemoryUsed { get; set; }

        /// <summary>
        /// 空闲内存MB或平均值
        /// </summary>
        public decimal MemoryFree { get; set; }

        /// <summary>
        /// 总内存MB固定值
        /// </summary>
        public decimal MemoryTotal { get; set; }

        /// <summary>
        /// 网卡入流量
        /// </summary>
        public decimal NetIn { get; set; }

        /// <summary>
        /// 网卡出流量
        /// </summary>
        public decimal NetOut { get; set; }
    }

    /// <summary>
    /// app当前运行分布
    /// </summary>
    public class OutputRunList
    {
        /// <summary>
        /// bindlinuxmodel.id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 服务器名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 服务器组名称
        /// </summary>
        public string gname { get; set; }

        /// <summary>
        /// 服务器ID
        /// </summary>
        public int linuxid { get; set; }

        /// <summary>
        /// 最小运行数量
        /// </summary>
        public int minnum { get; set; }

        /// <summary>
        /// 最大运行数量
        /// </summary>
        public int maxnum { get; set; }

        /// <summary>
        /// 当前运行数量
        /// </summary>
        public int runnum { get; set; }

        /// <summary>
        /// 是否绑定了
        /// </summary>
        public bool isenable { get; set; } = false;
    }

    /// <summary>
    /// 环境model绑定服务器，环境运行在哪个服务器上
    /// </summary>
    public class InputLinuxBindModel
    {
        /// <summary>
        /// 服务器ID
        /// </summary>
        public int linuxid { get; set; }

        /// <summary>
        /// 环境ID
        /// </summary>
        public int modelid { get; set; }

        /// <summary>
        /// 最小运行数量
        /// </summary>
        public int minnum { get; set; }

        /// <summary>
        /// 最大运行数量
        /// </summary>
        public int maxnum { get; set; }

        /// <summary>
        /// 绑定状态
        /// </summary>
        public bool isenable { get; set; }
    }


    public class InputLinuxBindRoute
    {
        /// <summary>
        /// 服务器ID
        /// </summary>
        public int linuxid { get; set; }

        /// <summary>
        /// 环境ID
        /// </summary>
        public int routeid { get; set; }

        /// <summary>
        /// 绑定状态
        /// </summary>
        public bool isenable { get; set; }
    }

    public class InputBuildUpdate
    {
        /// <summary>
        /// 构建到哪个仓储
        /// </summary>
        public int storeid { get; set; }

        /// <summary>
        /// 服务ID
        /// </summary>
        public int serviceid { get; set; }

        /// <summary>
        /// 多个环境，用逗号隔开
        /// </summary>
        public string models { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum CodeLevel
    {
        /// <summary>
        /// 登陆后才可继续操作 631
        /// </summary>
        NeedLogin = 631,
        /// <summary>
        /// 没有权限 403
        /// </summary>
        NotRole = 403,
        /// <summary>
        /// 需要登陆401
        /// </summary>
        UnLogin = 401,
        /// <summary>
        /// 显示信息给客户端
        /// </summary>
        ShowInfo = 630,
        /// <summary>
        /// 开发错误
        /// </summary>
        ApiError = 500
    }

    /// <summary>
    /// 
    /// </summary>
    public class PasteException : Exception
    {
        /// <summary>
        /// 错误码
        /// </summary>
        public int Code { get; set; } = (int)CodeLevel.ShowInfo;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="level"></param>
        public PasteException(string message, CodeLevel level = CodeLevel.ShowInfo) : base(message)
        {
            Code = (int)level;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="code"></param>
        public PasteException(string message, int code) : base(message)
        {
            Code = code;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ErrorInfoModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string message { get; set; }
    }


}
