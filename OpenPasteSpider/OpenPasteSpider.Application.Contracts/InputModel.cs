﻿using System.ComponentModel.DataAnnotations;

namespace OpenPasteSpider
{

    /// <summary>
    /// 关键字搜索
    /// </summary>
    public class InputWord
    {

        /// <summary>
        /// 页码
        /// </summary>
        public int page { get; set; } = 1;

        /// <summary>
        /// 页大小
        /// </summary>
        public int size { get; set; } = 20;

        /// <summary>
        /// 搜索关键字
        /// </summary>
        public string word { get; set; } = "";

    }

    public class InputAppSearch
    {

        /// <summary>
        /// 
        /// </summary>
        public int page { get; set; } = 1;

        /// <summary>
        /// 
        /// </summary>
        public int size { get; set; } = 20;

        /// <summary>
        /// 
        /// </summary>
        public int linuxid { get; set; } = 0;

        /// <summary>
        /// 状态
        /// </summary>
        public int state { get; set; } = 0;

        /// <summary>
        /// 服务名称
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string word { get; set; } = "";
    }

    public class BackModel
    {

        /// <summary>
        /// 
        /// </summary>
        public int Code { get; set; } = 200;

        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; } = "";

    }

    /// <summary>
    /// 登录 失败后限定时间
    /// </summary>
    public class InputLogin
    {

        /// <summary>
        /// 账号 手机或者邮箱
        /// </summary>
        [Required(ErrorMessage = "账号不能为空！")]
        public string account { get; set; }

        /// <summary>
        /// 密码，明文
        /// </summary>
        [Required(ErrorMessage = "密码不能为空！")]
        public string password { get; set; }

        /// <summary>
        /// 图形串号
        /// </summary>
        [RegularExpression("[a-z,0-9,A-Z]{16}", ErrorMessage = "图形串号错误，需要为16位数！")]
        [Required(ErrorMessage = "请求错误，图形串号不能为空！")]
        public string guid { get; set; }

        /// <summary>
        /// 图形码
        /// </summary>
        [RegularExpression("[a-z,0-9]{4,6}", ErrorMessage = "验证码为字母或者数字，4-6位数！")]
        [Required(ErrorMessage = "图形验证码不能为空！")]
        public string code { get; set; }

    }

    /// <summary>
    /// 
    /// </summary>
    public class InputBody
    {
        /// <summary>
        /// 数据的对象
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// 输入的内容体
        /// </summary>
        public string body { get; set; }

        /// <summary>
        /// 扩展 环境名称default
        /// </summary>
        public string model { get; set; }
    }

    public class InputSearchPlan
    {
        /// <summary>
        /// 
        /// </summary>
        public int projectid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int serviceid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int linuxid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int page { get; set; } = 1;

        /// <summary>
        /// 
        /// </summary>
        public int size { get; set; } = 20;

        /// <summary>
        /// 状态
        /// </summary>
        public RunState state { get; set; } = RunState.unknow;

    }

    public class InputSearchApp
    {
        /// <summary>
        /// 
        /// </summary>
        public int projectid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int serviceid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int modelid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int linuxid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int page { get; set; } = 1;
        /// <summary>
        /// 
        /// </summary>
        public int size { get; set; } = 20;
        /// <summary>
        /// 
        /// </summary>
        public string word { get; set; } = "";

        /// <summary>
        /// 运行状态过滤
        /// </summary>
        public RunState state { get; set; } = RunState.unknow;


    }

    /// <summary>
    /// 
    /// </summary>
    public class InputSearchImage
    {
        /// <summary>
        /// 
        /// </summary>
        public int storeid { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int page { get; set; } = 1;
        
        /// <summary>
        /// 
        /// </summary>
        public int size { get; set; } = 20;

        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string word { get; set; } = "";
        
        /// <summary>
        /// 
        /// </summary>
        public int serviceid { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int projectid { get; set; }
    }
}
