﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenPasteSpider
{
    ///// <summary>
    ///// 
    ///// </summary>
    //public class CommandInfo
    //{

    //    public string DockerRun { get; set; } = "docker run --name {{Service.AppPrefix}}{{App.Port}} -e TZ=\"Asia/Shanghai\"  -p {{App.Port}}:80 --restart=always -d {{Service.ImagePrefix}}:{{Service.ImageVersion}}";

    //    public string DockerBuild { get; set; } = "docker build -t {{Service.ImagePrefix}}:{{Service.ImageVersion}} {{Linux.WorkDir}}publish/{{Service.ServiceName}}/";

    //    public string DockerRm { get; set; } = "docker rm {{Service.AppPrefix}}{{App.Port}}";

    //    public string DockerRmi { get; set; } = "docker rmi {{Service.ServiceName}}:{{Service.ImageVersion}}";

    //    public string DockerStop { get; set; } = "docker stop {{Service.AppPrefix}}{{App.Port}}";

    //    public string SleepTime { get; set; } = "ping -c 5 127.0.0.1";

    //}

    /// <summary>
    /// Container运行的状态返回情况 通过docker stats读取
    /// </summary>
    public class DockerStateModel
    {
        /// <summary>
        /// 容器名称
        /// </summary>
        public string container { get; set; }

        /// <summary>
        /// 内存占用
        /// </summary>
        public string memory { get; set; }
        /// <summary>
        /// CPU占用
        /// </summary>
        public string cpu { get; set; }
        /// <summary>
        /// 线程或者进程
        /// </summary>
        public string pids { get; set; }
        /// <summary>
        /// 网络情况
        /// </summary>
        public string net { get; set; }

        /// <summary>
        /// CPU占用比率
        /// </summary>
        public double cpuval
        {
            get
            {
                if (!String.IsNullOrEmpty(cpu))
                {
                    double.TryParse(cpu.Replace("%", ""), out var cpudou);
                    return cpudou;
                }
                else
                {
                    return 0.0f;
                }
            }
        }

        /// <summary>
        /// 线程数量
        /// </summary>
        public int pidval { get { int.TryParse(pids, out var pint); return pint; } }

        /// <summary>
        /// 内存使用量（MB）
        /// </summary>
        public double memoryval
        {
            get
            {
                if (!String.IsNullOrEmpty(memory))
                {
                    var used = memory.Split('/')[0].Trim();
                    double returnval = 0.0f;
                    double.TryParse(used.Replace("MiB", "").Replace("KiB", "").Replace("GiB", ""), out var cpudou);
                    returnval = cpudou;
                    if (used.EndsWith("KiB"))
                    {
                        returnval = cpudou / 1024;
                    }
                    if (used.EndsWith("GiB"))
                    {
                        returnval = cpudou * 1024;
                    }
                    //更小的单位，直接忽略为0
                    return returnval;
                }
                else
                {
                    return 0.0f;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime time { get; set; } = DateTime.Now;


        /// <summary>
        /// 网络入流量
        /// </summary>
        public double netinval
        {
            get
            {

                if (!String.IsNullOrEmpty(net))
                {
                    var used = net.Split('/')[0].Trim();
                    double returnval = 0.0f;
                    double.TryParse(used.Replace("TB", "").Replace("GB", "").Replace("MB", ""), out var cpudou);
                    returnval = cpudou;
                    if (used.EndsWith("GB"))
                    {
                        returnval = cpudou * 1024;
                    }
                    if (used.EndsWith("TB"))
                    {
                        returnval = cpudou * 1024 * 1024;
                    }
                    //更小的单位，直接忽略为0 kB B
                    return returnval;
                }
                else
                {
                    return 0.0f;
                }

            }
        }

        /// <summary>
        /// 网络出流量
        /// </summary>
        public double netoutval
        {
            get
            {
                if (!String.IsNullOrEmpty(net))
                {
                    var used = net.Split('/')[1].Trim();
                    double returnval = 0.0f;
                    double.TryParse(used.Replace("TB", "").Replace("GB", "").Replace("MB", ""), out var cpudou);
                    returnval = cpudou;
                    if (used.EndsWith("GB"))
                    {
                        returnval = cpudou * 1024;
                    }
                    if (used.EndsWith("TB"))
                    {
                        returnval = cpudou * 1024 * 1024;
                    }
                    //更小的单位，直接忽略为0 kB B
                    return returnval;
                }
                else
                {
                    return 0.0f;
                }
            }
        }

        /// <summary>
        /// 暂存变量
        /// </summary>
        public string appprefix { get; set; }

        /// <summary>
        /// contaienrid 12位数
        /// </summary>
        public string idcode { get; set; }
    }

    /// <summary>
    /// 查看人容器运行列表 ps -a
    /// </summary>
    public class DockerRunList
    {

        /// <summary>
        /// 容器名称
        /// </summary>
        public string container { get; set; }
        /// <summary>
        /// 运行状态
        /// </summary>
        public string runstate { get; set; }

        /// <summary>
        /// 是否正在运行
        /// </summary>
        public bool isrunning { get; set; } = false;

        /// <summary>
        /// 监听端口
        /// </summary>
        public string ports { get; set; }

        /// <summary>
        /// 容器命名前缀
        /// </summary>
        public string apprefix { get; set; }

        /// <summary>
        /// 容器12长度id
        /// </summary>
        public string appid { get; set; }

    }

    /// <summary>
    /// docker inspect info
    /// </summary>
    public class DockerInspectModel
    {

        /// <summary>
        /// State.Status
        /// </summary>
        public string Status { get; set; } = "";

        /// <summary>
        /// NetworkSettings.IPAddress
        /// </summary>
        public string IPAddress { get; set; } = "";

        /// <summary>
        /// 
        /// </summary>
        public string SecondIpAddress { get; set; } = "";

        /// <summary>
        /// 监听的多个端口
        /// </summary>
        public string Ports { get; set; } = "";

        /// <summary>
        /// Config.Image
        /// </summary>
        public string Image { get; set; } = "";


        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; } = "";

        /// <summary>
        /// 重启次数
        /// </summary>
        public int RestartCount { get; set; } = 0;

        /// <summary>
        /// HostConfig:PortBindings:80/tcp:HostPort
        /// </summary>
        public int HostPort { get; set; } = 0;

        /// <summary>
        /// 容器文件路径
        /// </summary>
        public string MergedDir { get; set; } = "";

    }

    /// <summary>
    /// 卸载，降配的时候使用的数据模型
    /// </summary>
    public class MinusInfoModel
    {

        /// <summary>
        /// 哪台服务器
        /// </summary>
        public int linuxid { get; set; }

        ///// <summary>
        ///// 哪个服务? 无用参数
        ///// </summary>
        //public int serviceid { get; set; }

        /// <summary>
        /// 当前运行总数
        /// </summary>
        public int totalrunnum { get; set; }

        /// <summary>
        /// 至少保持量
        /// </summary>
        public int totalkeepnum { get; set; }

        /// <summary>
        /// 可以卸载量
        /// </summary>
        public int canminusnum { get; set; }

        /// <summary>
        /// 需要卸载量
        /// </summary>
        public int needminusnum { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int projectid { get; set; }

    }


    /// <summary>
    /// AppPoint的搜索条件
    /// </summary>
    public class AppInputSearchModel
    {

        /// <summary>
        /// docker前缀搜索
        /// </summary>
        public string AppPrefix { get; set; } = "";

        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string KeyWord { get; set; } = "";

        /// <summary>
        /// 哪一台服务器上的
        /// </summary>
        public int LinuxId { get; set; } = 0;

        /// <summary>
        /// 运行状态
        /// </summary>
        public int RunState { get; set; } = -1;

    }

    /// <summary>
    /// 关键字条件查询
    /// </summary>
    public class InputSearch
    {

        /// <summary>
        /// 页
        /// </summary>
        public int page { get; set; } = 1;

        /// <summary>
        /// 页大小
        /// </summary>
        public int size { get; set; } = 30;

        /// <summary>
        /// 关键字 containername
        /// </summary>
        public string Word { get; set; } = "";

        /// <summary>
        /// containerprefix
        /// </summary>
        public string Word2 { get; set; } = "";

        /// <summary>
        /// 对象ID linuxid
        /// </summary>
        public int objectid { get; set; } = 0;
    }

    public class InputMonitorSearch
    {

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime datestart { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime dateend { get; set; }

        /// <summary>
        /// 对象名称 containername linuxid
        /// </summary>
        public string Word { get; set; } = "";

        /// <summary>
        /// 对象ID linuxid
        /// </summary>
        public int objectid { get; set; } = 0;

        /// <summary>
        /// Containerid
        /// </summary>
        public int AppId { get; set; }

    }

    /// <summary>
    /// 按照条件获取报表数据
    /// </summary>
    public class InputReportSearch
    {
        /// <summary>
        /// 容器
        /// </summary>
        public int appid { get; set; }

        /// <summary>
        /// 服务器
        /// </summary>
        public int linuxid { get; set; }

        /// <summary>
        /// 时间类型 hour实时 week本周 prevweek上周 day今日
        /// </summary>
        public string datetype { get; set; } = "hour";
    }




}
