﻿using OpenPasteSpider.projectmodel;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.sourcemodel
{
    public class AsyncConfigAddDto
    {

        ///<summary>
        ///项目所属
        ///</summary>

        public int ServiceId { get; set; }

        ///<summary>
        ///文件或者文件夹全名称
        ///</summary>

        public string FileName { get; set; }

        ///<summary>
        ///是否文件夹
        ///</summary>

        public bool IsDirectory { get; set; }

        ///<summary>
        ///忽略或者必须
        ///</summary>

        public bool Ignore { get; set; }

        /////<summary>
        /////文件模式
        /////</summary>
        //public int FileModel { get; set; } = 2;

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
    }

    public class AsyncConfigDto : EntityDto<int>
    {

        ///<summary>
        ///项目所属
        ///</summary>

        public int ServiceId { get; set; }

        ///<summary>
        ///文件或者文件夹全名称
        ///</summary>

        public string FileName { get; set; }

        ///<summary>
        ///是否文件夹
        ///</summary>

        public bool IsDirectory { get; set; }

        ///<summary>
        ///忽略或者必须
        ///</summary>

        public bool Ignore { get; set; }

        ///<summary>
        ///文件模式
        ///</summary>

        public int FileModel { get; set; }

        public bool IsEnable { get; set; } = true;
    }
    public class AsyncConfigListDto : EntityDto<int>
    {


        public ServiceInfoListDto Service { get; set; }

        ///<summary>
        ///文件或者文件夹全名称 /开头
        ///</summary>
        public string FileName { get; set; }

        ///<summary>
        ///是否文件夹
        ///</summary>
        public bool IsDirectory { get; set; }

        ///<summary>
        ///忽略或者必须 否则为必须更替
        ///</summary>
        public bool Ignore { get; set; }

        /// <summary>
        /// 扩展 项目名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 文件模式 1源码 2发布文件
        /// </summary>
        public int FileModel { get; set; } = 2;

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
    }
    public class AsyncConfigUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///项目所属
        ///</summary>
        public int ServiceId { get; set; }

        ///<summary>
        ///文件或者文件夹全名称
        ///</summary>
        public string FileName { get; set; }

        ///<summary>
        ///是否文件夹
        ///</summary>
        public bool IsDirectory { get; set; }

        ///<summary>
        ///忽略或者必须
        ///</summary>
        public bool Ignore { get; set; }

        /////<summary>
        /////文件模式
        /////</summary>
        //public int FileModel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEnable { get; set; } = true;
    }
}
