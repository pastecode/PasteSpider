﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.monitor
{
    public class DockerTimeStateDto : EntityDto<int>
    {

        ///<summary>
        ///属于哪个服务
        ///</summary>
        [MaxLength(20)]
        [DefaultValue("去除端口后的名称")]
        public string AppPrefix { get; set; }

        ///<summary>
        ///数据时间yyyy-MM-dd
        ///</summary>

        public DateTime DataDate { get; set; }

        ///<summary>
        ///容器名称
        ///</summary>
        [MaxLength(30)]
        public string DockerName { get; set; }

        ///<summary>
        ///内存占用MB
        ///</summary>

        public decimal MemoryUsed { get; set; }

        ///<summary>
        ///CPU数据
        ///</summary>

        public decimal CPUUsed { get; set; }

        ///<summary>
        ///线程数量
        ///</summary>

        public int PIDSNum { get; set; }

        ///<summary>
        ///哪一台服务器的数据
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///最高CPU占用
        ///</summary>

        public decimal HCPU { get; set; }

        ///<summary>
        ///最低CPU占用
        ///</summary>

        public decimal LCPU { get; set; }

        ///<summary>
        ///最高内存占用
        ///</summary>

        public decimal HMemory { get; set; }

        ///<summary>
        ///最低内存占用
        ///</summary>

        public decimal LMemory { get; set; }

        ///<summary>
        ///最高线程数
        ///</summary>

        public int HPIDS { get; set; }

        ///<summary>
        ///最低线程数
        ///</summary>

        public int LPIDS { get; set; }

        ///<summary>
        ///MB单位流量入
        ///</summary>

        public decimal NetIn { get; set; }

        ///<summary>
        ///MB单位流量出
        ///</summary>

        public decimal NetOut { get; set; }
    }
    public class DockerTimeStateListDto : EntityDto<int>
    {

        ///<summary>
        ///属于哪个服务
        ///</summary>
        [MaxLength(20)]
        [DefaultValue("去除端口后的名称")]
        public string AppPrefix { get; set; }

        ///<summary>
        ///数据时间yyyy-MM-dd
        ///</summary>

        public DateTime DataDate { get; set; }

        ///<summary>
        ///容器名称
        ///</summary>
        [MaxLength(30)]
        public string DockerName { get; set; }

        ///<summary>
        ///内存占用MB
        ///</summary>

        public decimal MemoryUsed { get; set; }

        ///<summary>
        ///CPU数据
        ///</summary>

        public decimal CPUUsed { get; set; }

        ///<summary>
        ///线程数量
        ///</summary>

        public int PIDSNum { get; set; }

        ///<summary>
        ///哪一台服务器的数据
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///最高CPU占用
        ///</summary>

        public decimal HCPU { get; set; }

        ///<summary>
        ///最低CPU占用
        ///</summary>

        public decimal LCPU { get; set; }

        ///<summary>
        ///最高内存占用
        ///</summary>

        public decimal HMemory { get; set; }

        ///<summary>
        ///最低内存占用
        ///</summary>

        public decimal LMemory { get; set; }

        ///<summary>
        ///最高线程数
        ///</summary>

        public int HPIDS { get; set; }

        ///<summary>
        ///最低线程数
        ///</summary>

        public int LPIDS { get; set; }

        ///<summary>
        ///MB单位流量入
        ///</summary>

        public decimal NetIn { get; set; }

        ///<summary>
        ///MB单位流量出
        ///</summary>

        public decimal NetOut { get; set; }
    }
}
