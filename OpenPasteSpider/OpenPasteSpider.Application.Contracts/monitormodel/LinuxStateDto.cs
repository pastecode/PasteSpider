﻿using System;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.monitor
{
    public class LinuxStateDto : EntityDto<int>
    {

        ///<summary>
        ///哪一台服务器的
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///数据时间
        ///</summary>

        public DateTime DataDate { get; set; }

        ///<summary>
        ///CPU当前使用量或平均值
        ///</summary>

        public decimal CPUUsed { get; set; }

        ///<summary>
        ///1分钟CPU平均值
        ///</summary>

        public decimal CPU1 { get; set; }

        ///<summary>
        ///5分钟CPU平均值
        ///</summary>

        public decimal CPU2 { get; set; }

        ///<summary>
        ///15分钟CPU平均值
        ///</summary>

        public decimal CPU3 { get; set; }

        ///<summary>
        ///使用内存MB或平均值
        ///</summary>

        public decimal MemoryUsed { get; set; }

        ///<summary>
        ///空闲内存MB或平均值
        ///</summary>

        public decimal MemoryFree { get; set; }

        ///<summary>
        ///总内存MB固定值
        ///</summary>

        public decimal MemoryTotal { get; set; }

        ///<summary>
        ///网卡入流量
        ///</summary>

        public decimal NetIn { get; set; }

        ///<summary>
        ///网卡出流量
        ///</summary>

        public decimal NetOut { get; set; }
    }
    public class LinuxStateListDto : EntityDto<int>
    {

        ///<summary>
        ///哪一台服务器的
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///数据时间
        ///</summary>

        public DateTime DataDate { get; set; }

        ///<summary>
        ///CPU当前使用量或平均值
        ///</summary>

        public decimal CPUUsed { get; set; }

        ///<summary>
        ///1分钟CPU平均值
        ///</summary>

        public decimal CPU1 { get; set; }

        ///<summary>
        ///5分钟CPU平均值
        ///</summary>

        public decimal CPU2 { get; set; }

        ///<summary>
        ///15分钟CPU平均值
        ///</summary>

        public decimal CPU3 { get; set; }

        ///<summary>
        ///使用内存MB或平均值
        ///</summary>

        public decimal MemoryUsed { get; set; }

        ///<summary>
        ///空闲内存MB或平均值
        ///</summary>

        public decimal MemoryFree { get; set; }

        ///<summary>
        ///总内存MB固定值
        ///</summary>

        public decimal MemoryTotal { get; set; }

        ///<summary>
        ///网卡入流量
        ///</summary>

        public decimal NetIn { get; set; }

        ///<summary>
        ///网卡出流量
        ///</summary>

        public decimal NetOut { get; set; }
    }
}
