﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.monitor
{
    public class DockerStateDto : EntityDto<int>
    {

        ///<summary>
        ///去除版本号后的名称
        ///</summary>
        [MaxLength(20)]
        [DefaultValue("去除版本号后的名称")]
        public string AppPrefix { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AppShortId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModelCode { get; set; }

        /// <summary>
        /// 哪个服务
        /// </summary>
        public int ServiceId { get; set; }

        ///<summary>
        ///数据时间yyyy-MM-dd
        ///</summary>

        public DateTime DataDate { get; set; }

        ///<summary>
        ///容器名称
        ///</summary>
        [MaxLength(30)]
        public string DockerName { get; set; }

        ///<summary>
        ///内存占用MB
        ///</summary>

        public decimal MemoryUsed { get; set; }

        ///<summary>
        ///CPU数据
        ///</summary>

        public decimal CPUUsed { get; set; }

        ///<summary>
        ///线程数量
        ///</summary>

        public int PIDSNum { get; set; }

        ///<summary>
        ///哪一台服务器的数据
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///MB单位流量入
        ///</summary>

        public decimal NetIn { get; set; }

        ///<summary>
        ///MB单位流量出
        ///</summary>

        public decimal NetOut { get; set; }
    }
    public class DockerStateListDto : EntityDto<int>
    {

        ///<summary>
        ///去除版本号后的名称
        ///</summary>
        [MaxLength(20)]
        [DefaultValue("去除版本号后的名称")]
        public string AppPrefix { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AppShortId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModelCode { get; set; }

        /// <summary>
        /// 哪个服务
        /// </summary>
        public int ServiceId { get; set; }


        ///<summary>
        ///数据时间yyyy-MM-dd
        ///</summary>

        public DateTime DataDate { get; set; }

        ///<summary>
        ///容器名称
        ///</summary>
        [MaxLength(30)]
        public string DockerName { get; set; }

        ///<summary>
        ///内存占用MB
        ///</summary>

        public decimal MemoryUsed { get; set; }

        ///<summary>
        ///CPU数据
        ///</summary>

        public decimal CPUUsed { get; set; }

        ///<summary>
        ///线程数量
        ///</summary>

        public int PIDSNum { get; set; }

        ///<summary>
        ///哪一台服务器的数据
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///MB单位流量入
        ///</summary>

        public decimal NetIn { get; set; }

        ///<summary>
        ///MB单位流量出
        ///</summary>

        public decimal NetOut { get; set; }
    }
}
