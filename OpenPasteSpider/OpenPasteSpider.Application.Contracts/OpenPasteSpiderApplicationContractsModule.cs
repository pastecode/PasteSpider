﻿using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace OpenPasteSpider
{
    [DependsOn(
        typeof(AbpDddApplicationContractsModule)
        )]
    public class OpenPasteSpiderApplicationContractsModule : AbpModule
    {

    }
}
