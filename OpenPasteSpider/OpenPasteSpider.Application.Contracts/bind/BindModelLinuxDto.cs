﻿using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.bind
{
    public class BindModelLinuxAddDto
    {

        ///<summary>
        ///模型
        ///</summary>

        public int ModelId { get; set; }

        ///<summary>
        ///服务器
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///当前运行数
        ///</summary>

        public int RunNum { get; set; }

        ///<summary>
        ///最小运行量
        ///</summary>

        public int LimitMinNum { get; set; }

        ///<summary>
        ///最大运行量
        ///</summary>

        public int LimitMaxNum { get; set; }

        ///<summary>
        ///状态
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///权重
        ///</summary>

        public int Weight { get; set; }
    }
    public class BindModelLinuxDto : EntityDto<int>
    {

        ///<summary>
        ///模型
        ///</summary>
        public int ModelId { get; set; }

        ///<summary>
        ///服务器
        ///</summary>
        public int LinuxId { get; set; }

        ///<summary>
        ///当前运行数
        ///</summary>
        public int RunNum { get; set; }

        ///<summary>
        ///最小运行量
        ///</summary>
        public int LimitMinNum { get; set; }

        ///<summary>
        ///最大运行量
        ///</summary>
        public int LimitMaxNum { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///权重
        ///</summary>
        public int Weight { get; set; }

        /// <summary>
        /// 环境的代码
        /// </summary>
        public string ModelCode { get; set; }

        /// <summary>
        /// 服务ID
        /// </summary>
        public int ServiceId { get; set; }
    }
    public class BindModelLinuxListDto : EntityDto<int>
    {

        ///<summary>
        ///模型
        ///</summary>

        public int ModelId { get; set; }

        ///<summary>
        ///服务器
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///当前运行数
        ///</summary>

        public int RunNum { get; set; }

        ///<summary>
        ///最小运行量
        ///</summary>

        public int LimitMinNum { get; set; }

        ///<summary>
        ///最大运行量
        ///</summary>

        public int LimitMaxNum { get; set; }

        ///<summary>
        ///状态
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///权重
        ///</summary>

        public int Weight { get; set; }
    }
    public class BindModelLinuxUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///模型
        ///</summary>

        public int ModelId { get; set; }

        ///<summary>
        ///服务器
        ///</summary>

        public int LinuxId { get; set; }

        ///<summary>
        ///当前运行数
        ///</summary>

        public int RunNum { get; set; }

        ///<summary>
        ///最小运行量
        ///</summary>

        public int LimitMinNum { get; set; }

        ///<summary>
        ///最大运行量
        ///</summary>

        public int LimitMaxNum { get; set; }

        ///<summary>
        ///状态
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///权重
        ///</summary>

        public int Weight { get; set; }
    }
}
