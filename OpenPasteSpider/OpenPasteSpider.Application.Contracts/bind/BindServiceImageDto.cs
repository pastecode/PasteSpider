﻿using System;
using System.ComponentModel.DataAnnotations;
using OpenPasteSpider.projectmodel;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.bind
{
    public class BindServiceImageAddDto
    {

        ///<summary>
        ///属于哪一个服务的包
        ///</summary>

        public int ServiceId { get; set; }

        ///<summary>
        ///使用的哪一个镜像库
        ///</summary>

        public int StoreId { get; set; }

        ///<summary>
        ///当前版本
        ///</summary>

        public int Version { get; set; }

        ///<summary>
        ///是否可用
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///文件SHA256码
        ///</summary>
        [MaxLength(300)]
        public string ImageDigest { get; set; }

        ///<summary>
        ///0待构建
        ///</summary>

        public RunState BuildState { get; set; }
    }
    public class BindServiceImageDto : EntityDto<int>
    {

        ///<summary>
        ///属于哪一个服务的包
        ///</summary>

        public int ServiceId { get; set; }

        ///<summary>
        ///使用的哪一个镜像库
        ///</summary>

        public int StoreId { get; set; }

        ///<summary>
        ///当前版本
        ///</summary>

        public int Version { get; set; }

        ///<summary>
        ///是否可用
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///构建时间
        ///</summary>

        public DateTime CreateDate { get; set; }

        ///<summary>
        ///文件SHA256码
        ///</summary>
        [MaxLength(300)]
        public string ImageDigest { get; set; }

        ///<summary>
        ///0待构建
        ///</summary>

        public RunState BuildState { get; set; }
    }
    public class BindServiceImageListDto : EntityDto<int>
    {

        ///<summary>
        ///属于哪一个服务的包
        ///</summary>

        public int ServiceId { get; set; }

        /// <summary>
        /// 扩展 服务信息
        /// </summary>
        public ServiceInfoDto Service { get; set; }

        ///<summary>
        ///使用的哪一个镜像库
        ///</summary>
        public int StoreId { get; set; }

        ///<summary>
        ///当前版本
        ///</summary>
        public int Version { get; set; }

        ///<summary>
        ///是否可用
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///构建时间
        ///</summary>
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///文件SHA256码
        ///</summary>
        [MaxLength(300)]
        public string ImageDigest { get; set; }

        ///<summary>
        ///0待构建
        ///</summary>
        public RunState BuildState { get; set; }
    }
    public class BindServiceImageUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///属于哪一个服务的包
        ///</summary>

        public int ServiceId { get; set; }

        ///<summary>
        ///使用的哪一个镜像库
        ///</summary>

        public int StoreId { get; set; }

        ///<summary>
        ///当前版本
        ///</summary>

        public int Version { get; set; }

        ///<summary>
        ///是否可用
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///文件SHA256码
        ///</summary>
        [MaxLength(300)]
        public string ImageDigest { get; set; }

        ///<summary>
        ///0待构建
        ///</summary>

        public RunState BuildState { get; set; }
    }

}
