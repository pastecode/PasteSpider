﻿using System;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.usermodel
{
    public class UserInfoAddDto
    {

        ///<summary>
        ///用户名
        ///</summary>
        [MaxLength(32)]
        public string UserName { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///邮箱
        ///</summary>
        [MaxLength(64)]
        public string Email { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        [MaxLength(64)]
        public string PassWord { get; set; }


        ///<summary>
        ///哪个角色？
        ///</summary>
        [MaxLength(16)]
        public string Grade { get; set; }
    }
    public class UserInfoDto : EntityDto<int>
    {

        ///<summary>
        ///用户名
        ///</summary>
        [MaxLength(32)]
        public string UserName { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>

        public DateTime CreateDate { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///邮箱
        ///</summary>
        [MaxLength(64)]
        public string Email { get; set; }


        ///<summary>
        ///状态
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///哪个角色？
        ///</summary>
        [MaxLength(16)]
        public string Grade { get; set; }
    }
    public class UserInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///用户名
        ///</summary>
        [MaxLength(32)]
        public string UserName { get; set; }

        ///<summary>
        ///创建日期
        ///</summary>
        public DateTime CreateDate { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///邮箱
        ///</summary>
        [MaxLength(64)]
        public string Email { get; set; }


        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///哪个角色？
        ///</summary>
        [MaxLength(16)]
        public string Grade { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class UserInfoUpdateDto : EntityDto<int>
    {

        ///<summary>
        ///用户名
        ///</summary>
        [MaxLength(32)]
        public string UserName { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///邮箱
        ///</summary>
        [MaxLength(64)]
        public string Email { get; set; }

        ///<summary>
        ///密码
        ///</summary>
        [MaxLength(64)]
        public string PassWord { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///哪个角色？
        ///</summary>
        [MaxLength(16)]
        public string Grade { get; set; }
    }
}
