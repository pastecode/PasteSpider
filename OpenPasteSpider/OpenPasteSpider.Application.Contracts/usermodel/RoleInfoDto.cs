﻿using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace OpenPasteSpider.usermodel
{
    public class RoleInfoAddDto
    {
        /// <summary>
        /// 父级ID
        /// </summary>
        public int FatherId { get; set; } = 0;

        /// <summary>
        /// 权限类型 0权限 1菜单 2按钮
        /// </summary>
        public int RoleType { get; set; } = 0;

        ///<summary>
        ///权限模块
        ///</summary>
        [MaxLength(16)]
        public string Model { get; set; }

        ///<summary>
        ///权限项
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        /// <summary>
        /// 权限
        /// </summary>
        [MaxLength(64)]
        public string Role { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        public int Sort { get; set; }
    }
    public class RoleInfoDto : EntityDto<int>
    {

        ///<summary>
        ///名称
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        ///<summary>
        ///权限模块
        ///</summary>
        [MaxLength(16)]
        public string Model { get; set; }

        /// <summary>
        /// 权限
        /// </summary>
        public string Role { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        public int Sort { get; set; }

        /// <summary>
        /// 权限类型 0权限 1菜单 2按钮
        /// </summary>
        public int RoleType { get; set; } = 0;
    }
    public class RoleInfoListDto : EntityDto<int>
    {

        ///<summary>
        ///权限模块
        ///</summary>
        [MaxLength(16)]
        public string Model { get; set; }

        ///<summary>
        ///权限项
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        public string Role { get; set; }
        ///<summary>
        ///状态
        ///</summary>
        public bool IsEnable { get; set; }

        ///<summary>
        ///排序
        ///</summary>
        public int Sort { get; set; }

        /// <summary>
        /// 权限类型 0权限 1菜单 2按钮
        /// </summary>
        public int RoleType { get; set; } = 0;
        /// <summary>
        /// 扩展 角色名称
        /// </summary>
        public string Grade { get; set; } = "";
    }
    public class RoleInfoUpdateDto : EntityDto<int>
    {
        /// <summary>
        /// 父级ID
        /// </summary>
        public int FatherId { get; set; } = 0;

        /// <summary>
        /// 权限类型 0权限 1菜单 2按钮
        /// </summary>
        public int RoleType { get; set; } = 0;
        ///<summary>
        ///权限模块
        ///</summary>
        [MaxLength(16)]
        public string Model { get; set; }
        /// <summary>
        /// 权限
        /// </summary>
        [MaxLength(64)]
        public string Role { get; set; }
        ///<summary>
        ///权限项
        ///</summary>
        [MaxLength(16)]
        public string Name { get; set; }

        ///<summary>
        ///描述
        ///</summary>
        [MaxLength(64)]
        public string Desc { get; set; }

        ///<summary>
        ///状态
        ///</summary>

        public bool IsEnable { get; set; }

        ///<summary>
        ///排序
        ///</summary>

        public int Sort { get; set; }
    }
}
