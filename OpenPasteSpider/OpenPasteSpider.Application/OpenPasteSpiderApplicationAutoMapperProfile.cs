﻿using AutoMapper;
using OpenPasteSpider.bind;
using OpenPasteSpider.monitor;
using OpenPasteSpider.monitormodel;
using OpenPasteSpider.noticemodels;
using OpenPasteSpider.projectmodel;
using OpenPasteSpider.sourcemodel;
using OpenPasteSpider.usermodel;

namespace OpenPasteSpider
{
    public class OpenPasteSpiderApplicationAutoMapperProfile : Profile
    {
        public OpenPasteSpiderApplicationAutoMapperProfile()
        {


            // #ServiceInfo#
            CreateMap<ServiceInfo, ServiceInfoListDto>();
            CreateMap<ServiceInfo, ServiceInfoDto>();
            CreateMap<ServiceInfo, ServiceInfoUpdateDto>();
            CreateMap<ServiceInfoUpdateDto, ServiceInfo>();
            CreateMap<ServiceInfoAddDto, ServiceInfo>();


            // #NameInfo#
            CreateMap<ProjectInfo, ProjectInfoDto>();
            CreateMap<ProjectInfo, ProjectInfoDto>();
            CreateMap<ProjectInfo, ProjectInfoUpdateDto>();
            CreateMap<ProjectInfoUpdateDto, ProjectInfo>();
            CreateMap<ProjectInfoAddDto, ProjectInfo>();

            // #ModelInfo#
            CreateMap<ModelInfo, ModelInfoListDto>();
            CreateMap<ModelInfo, ModelInfoDto>();
            CreateMap<ModelInfo, ModelInfoUpdateDto>();
            CreateMap<ModelInfoUpdateDto, ModelInfo>();
            CreateMap<ModelInfoAddDto, ModelInfo>();

            // #LinuxInfo#
            CreateMap<LinuxInfo, LinuxInfoListDto>();
            CreateMap<LinuxInfo, LinuxInfoDto>();
            CreateMap<LinuxInfo, LinuxInfoUpdateDto>();
            CreateMap<LinuxInfoUpdateDto, LinuxInfo>();
            CreateMap<LinuxInfoAddDto, LinuxInfo>();

            // DockerTimeState:
            CreateMap<DockerTimeState, DockerTimeStateListDto>();
            CreateMap<DockerTimeState, DockerTimeStateDto>();
            //CreateMap<DockerTimeState, DockerTimeStateUpdateDto>();
            //CreateMap<DockerTimeStateUpdateDto, DockerTimeState>();
            //CreateMap<DockerTimeStateAddDto, DockerTimeState>();

            // DockerState:
            CreateMap<DockerState, DockerStateListDto>();
            CreateMap<DockerState, DockerStateDto>();
            //CreateMap<DockerState, DockerStateUpdateDto>();
            //CreateMap<DockerStateUpdateDto, DockerState>();
            //CreateMap<DockerStateAddDto, DockerState>();

            // LinuxState:
            CreateMap<LinuxState, LinuxStateListDto>();
            CreateMap<LinuxState, LinuxStateDto>();
            //CreateMap<LinuxState, LinuxStateUpdateDto>();
            //CreateMap<LinuxStateUpdateDto, LinuxState>();
            //CreateMap<LinuxStateAddDto, LinuxState>();

            // LinuxTimeState:
            CreateMap<LinuxTimeState, LinuxTimeStateListDto>();
            CreateMap<LinuxTimeState, LinuxTimeStateDto>();
            //CreateMap<LinuxTimeState, LinuxTimeStateUpdateDto>();
            //CreateMap<LinuxTimeStateUpdateDto, LinuxTimeState>();
            //CreateMap<LinuxTimeStateAddDto, LinuxTimeState>();

            // #AppInfo#
            CreateMap<AppInfo, AppInfoListDto>();
            CreateMap<AppInfo, AppInfoDto>();

            // #PlanInfo#
            CreateMap<PlanInfo, PlanInfoListDto>();
            CreateMap<PlanInfo, PlanInfoDto>();

            // #PlanItem#
            CreateMap<PlanItem, PlanItemListDto>();
            CreateMap<PlanItem, PlanItemDto>();

            // #ProjectInfo#
            CreateMap<ProjectInfo, ProjectInfoListDto>();
            CreateMap<ProjectInfo, ProjectInfoDto>();
            CreateMap<ProjectInfo, ProjectInfoUpdateDto>();
            CreateMap<ProjectInfoUpdateDto, ProjectInfo>();
            CreateMap<ProjectInfoAddDto, ProjectInfo>();



            // #BindModelLinux#
            CreateMap<BindModelLinux, BindModelLinuxListDto>();
            CreateMap<BindModelLinux, BindModelLinuxDto>();
            CreateMap<BindModelLinux, BindModelLinuxUpdateDto>();
            CreateMap<BindModelLinuxUpdateDto, BindModelLinux>();
            CreateMap<BindModelLinuxAddDto, BindModelLinux>();


            // *AsyncConfig*:
            CreateMap<AsyncConfig, AsyncConfigListDto>();


            // *UserInfo*:
            CreateMap<UserInfo, UserInfoListDto>();
            CreateMap<UserInfo, UserInfoDto>();
            CreateMap<UserInfo, UserInfoUpdateDto>();
            CreateMap<UserInfoUpdateDto, UserInfo>();
            CreateMap<UserInfoAddDto, UserInfo>();

            // *RoleInfo*:
            CreateMap<RoleInfo, RoleInfoListDto>();
            CreateMap<RoleInfo, RoleInfoDto>();
            CreateMap<RoleInfo, RoleInfoUpdateDto>();
            CreateMap<RoleInfoUpdateDto, RoleInfo>();
            CreateMap<RoleInfoAddDto, RoleInfo>();

            // *GradeInfo*:
            CreateMap<GradeInfo, GradeInfoListDto>();
            CreateMap<GradeInfo, GradeInfoDto>();
            CreateMap<GradeInfo, GradeInfoUpdateDto>();
            CreateMap<GradeInfoUpdateDto, GradeInfo>();
            CreateMap<GradeInfoAddDto, GradeInfo>();

            // *GradeRole*:
            CreateMap<GradeRole, GradeRoleListDto>();
            CreateMap<GradeRole, GradeRoleDto>();
            CreateMap<GradeRole, GradeRoleUpdateDto>();
            CreateMap<GradeRoleUpdateDto, GradeRole>();
            CreateMap<GradeRoleAddDto, GradeRole>();



            // #AsyncConfig#
            CreateMap<AsyncConfig, AsyncConfigListDto>();
            CreateMap<AsyncConfig, AsyncConfigDto>();
            CreateMap<AsyncConfig, AsyncConfigUpdateDto>();
            CreateMap<AsyncConfigUpdateDto, AsyncConfig>();
            CreateMap<AsyncConfigAddDto, AsyncConfig>();

            // #BindServiceImage#
            CreateMap<BindServiceImage, BindServiceImageListDto>();
            CreateMap<BindServiceImage, BindServiceImageDto>();
            CreateMap<BindServiceImage, BindServiceImageUpdateDto>();
            CreateMap<BindServiceImageUpdateDto, BindServiceImage>();
            CreateMap<BindServiceImageAddDto, BindServiceImage>();


            // *RevicerInfo*:
            CreateMap<RevicerInfo, RevicerInfoListDto>();
            CreateMap<RevicerInfo, RevicerInfoDto>();
            CreateMap<RevicerInfo, RevicerInfoUpdateDto>();
            CreateMap<RevicerInfoUpdateDto, RevicerInfo>();
            CreateMap<RevicerInfoAddDto, RevicerInfo>();



        }
    }
}