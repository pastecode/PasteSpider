﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Uow;

namespace OpenPasteSpider.usermodel
{

    /// <summary>
    /// 角色分组
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class GradeInfoAppService : OpenPasteSpiderAppService
    {

        //private readonly IRepository<GradeInfo, int> _repository;

        private IOpenPasteSpiderDbContext _dbContext;

        public GradeInfoAppService(IOpenPasteSpiderDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<GradeInfoListDto>> GetListAsync(int page = 1, int size = 20)
        {

            //_dbContext.GradeInfo.Where
            var query = _dbContext.GradeInfo.Where(t => 1 == 1)
                .OrderByDescending(xy => xy.Id);

            PagedResultDto<GradeInfoListDto> pagedResultDto = new PagedResultDto<GradeInfoListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(page, size).ToListAsync();
            var temList = ObjectMapper.Map<List<GradeInfo>, List<GradeInfoListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 根据ID获取单项角色分组
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GradeInfoDto GetByIdAsync(int id)
        {
            var query = _dbContext.GradeInfo.Where(t => t.Id == id)
                .FirstOrDefault();
            var temList = ObjectMapper.Map<GradeInfo, GradeInfoDto>(query);
            return temList;
        }

        /// <summary>
        /// 根据ID获取待更新单项信息角色分组
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public GradeInfoUpdateDto GetInfoForUpdateAsync(int id)
        {
            var query = _dbContext.GradeInfo.Where(t => t.Id == id)
                .FirstOrDefault();
            var temList = ObjectMapper.Map<GradeInfo, GradeInfoUpdateDto>(query);
            return temList;
        }


        /// <summary>
        /// 添加一个角色分组
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<GradeInfoDto> CreateItemAsync(GradeInfoAddDto input)
        {

            var newu = ObjectMapper.Map<GradeInfoAddDto, GradeInfo>(input);
            newu.IsEnable = true;//添加自定义
            //var updated = await _repository.InsertAsync(newu, true);
            _dbContext.Add(newu);
            await _dbContext.SaveChangesAsync();
            var backinfo = ObjectMapper.Map<GradeInfo, GradeInfoDto>(newu);
            return backinfo;
        }
        /// <summary>
        /// 更新一个角色分组
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<GradeInfoDto> UpdateItemAsync(GradeInfoUpdateDto input)
        {
            var info = await _dbContext.GradeInfo.Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            if (info == null || info == default)
            {
                throw new UserFriendlyException("需要查询的信息不存在", "404");
            }
            ObjectMapper.Map<GradeInfoUpdateDto, GradeInfo>(input, info);
            await _dbContext.SaveChangesAsync();
            var backinfo = ObjectMapper.Map<GradeInfo, GradeInfoDto>(info);
            return backinfo;
        }

        ///// <summary>
        ///// 删除角色分组软删除
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<int> DeleteItemById(int id)
        //{
        //    var aione = await _repository.GetAsync(xy => xy.Id == id);//.FirstOrDefault();
        //    if (aione != default)
        //    {
        //        //await _repository.DeleteAsync(aione);
        //    }
        //    return 1;
        //}

    }
}
