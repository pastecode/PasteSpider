﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Uow;

namespace OpenPasteSpider.usermodel
{

    /// <summary>
    /// 用户信息
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class UserInfoAppService : OpenPasteSpiderAppService
    {

        //private readonly IRepository<UserInfo, int> _repository;

        private IOpenPasteSpiderDbContext _dbContext;

        public UserInfoAppService(

            IOpenPasteSpiderDbContext dbcontext
        )
        {
            _dbContext = dbcontext;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<UserInfoListDto>> GetListAsync(int page = 1, int size = 20)
        {

            var query = _dbContext.UserInfo.Where(t => 1 == 1)
                .OrderByDescending(xy => xy.Id);

            PagedResultDto<UserInfoListDto> pagedResultDto = new PagedResultDto<UserInfoListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(page, size).ToListAsync();
            var temList = ObjectMapper.Map<List<UserInfo>, List<UserInfoListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 根据ID获取单项用户信息
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public UserInfoDto GetByIdAsync(int id)
        {
            var query = _dbContext.UserInfo.Where(t => t.Id == id)
                .FirstOrDefault();
            var temList = ObjectMapper.Map<UserInfo, UserInfoDto>(query);
            return temList;
        }

        /// <summary>
        /// 根据ID获取待更新单项信息用户信息
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public UserInfoDto GetInfoForUpdateAsync(int id)
        {
            var query = _dbContext.UserInfo.Where(t => t.Id == id)
                .FirstOrDefault();
            var temList = ObjectMapper.Map<UserInfo, UserInfoDto>(query);
            return temList;
        }


        /// <summary>
        /// 添加一个用户信息
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<UserInfoDto> CreateItemAsync(UserInfoAddDto input)
        {

            var newu = ObjectMapper.Map<UserInfoAddDto, UserInfo>(input);
            newu.CreateDate = DateTimeOffset.UtcNow.DateTime;
            newu.IsEnable = true;//添加自定义
            newu.PassWord = input.PassWord.ToMd5Lower();
            //var updated = await _repository.InsertAsync(newu, true);
            _dbContext.Add(newu);
            await _dbContext.SaveChangesAsync();
            var backinfo = ObjectMapper.Map<UserInfo, UserInfoDto>(newu);
            return backinfo;
        }
        /// <summary>
        /// 更新一个用户信息
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [UnitOfWork(false)]
        [HttpPost]
        public async Task<UserInfoDto> UpdateItemAsync(UserInfoUpdateDto input)
        {
            var info = await _dbContext.UserInfo.Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            if (info == null || info == default)
            {
                throw new UserFriendlyException("需要查询的信息不存在", "404");
            }
            var oldpass = info.PassWord;
            ObjectMapper.Map<UserInfoUpdateDto, UserInfo>(input, info);
            //var updated = await _repository.UpdateAsync(newu);
            if (!String.IsNullOrEmpty(input.PassWord))
            {
                info.PassWord = input.PassWord.ToMd5Lower();
            }
            else
            {
                info.PassWord = oldpass;
            }
            await _dbContext.SaveChangesAsync();
            var backinfo = ObjectMapper.Map<UserInfo, UserInfoDto>(info);
            return backinfo;
        }

        ///// <summary>
        ///// 删除用户信息软删除
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<int> DeleteItemById(int id)
        //{
        //    var aione = await _repository.GetAsync(xy => xy.Id == id);//.FirstOrDefault();
        //    if (aione != default)
        //    {
        //        //await _repository.DeleteAsync(aione);
        //    }
        //    return 1;
        //}

    }
}
