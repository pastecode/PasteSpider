﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OpenPasteSpider.monitor;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace OpenPasteSpider.monitormodel
{

    /// <summary>
    /// Linux机器的CPU和内存情况
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class LinuxStateAppService : OpenPasteSpiderAppService
    {

        //private readonly IRepository<LinuxState, long> _repository;

        private readonly IOpenPasteSpiderDbContext _dbContext;

        public LinuxStateAppService(IOpenPasteSpiderDbContext dbContext
        )
        {
            //_repository = repository;
            _dbContext = dbContext;
        }
        /// <summary>
        /// 按页获取Linux机器的CPU和内存情况
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<LinuxStateListDto>> GetListAsync(InputSearch input)
        {


            var query = _dbContext.LinuxState.Where(t => 1 == 1)
                .WhereIf(input.objectid != 0, x => x.LinuxId == input.objectid)
                .AsNoTracking()
                .OrderBy(xy => xy.Id);
            PagedResultDto<LinuxStateListDto> pagedResultDto = new PagedResultDto<LinuxStateListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(input.page, input.size).ToListAsync();
            var temList = ObjectMapper.Map<List<LinuxState>, List<LinuxStateListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 读取图表数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<LinuxStateListDto>> ListCharts(InputMonitorSearch input)
        {

            var query = _dbContext.LinuxState.Where(t => 1 == 1)
                .WhereIf(input.objectid != 0, x => x.LinuxId == input.objectid)
                .Where(x => x.DataDate >= input.datestart && x.DataDate <= input.dateend)
                .OrderBy(xy => xy.DataDate);
            PagedResultDto<LinuxStateListDto> pagedResultDto = new PagedResultDto<LinuxStateListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.ToListAsync();
            var temList = ObjectMapper.Map<List<LinuxState>, List<LinuxStateListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }


        ///// <summary>
        ///// 根据ID获取单项Linux机器的CPU和内存情况
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public LinuxStateDto GetByIdAsync(int id)
        //{
        //    var query = _repository.Where(t => t.Id == id)
        //        .FirstOrDefault();
        //    var temList = ObjectMapper.Map<LinuxState, LinuxStateDto>(query);
        //    return temList;
        //}

        ///// <summary>
        ///// 根据ID获取待更新单项信息Linux机器的CPU和内存情况
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public LinuxStateUpdateDto GetInfoForUpdateAsync(int id)
        //{
        //    var query = _repository.Where(t => t.Id == id)
        //        .FirstOrDefault();
        //    var temList = ObjectMapper.Map<LinuxState, LinuxStateUpdateDto>(query);
        //    return temList;
        //}


        ///// <summary>
        ///// 添加一个Linux机器的CPU和内存情况
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[UnitOfWork(false)]
        //[HttpPost]
        //public async Task<LinuxStateDto> CreateItemAsync(LinuxStateAddDto input)
        //{

        //    var newu = ObjectMapper.Map<LinuxStateAddDto, LinuxState>(input);
        //    //添加自定义
        //    var updated = await _repository.InsertAsync(newu, true);
        //    var backinfo = ObjectMapper.Map<LinuxState, LinuxStateDto>(updated);
        //    return backinfo;
        //}
        ///// <summary>
        ///// 更新一个Linux机器的CPU和内存情况
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[UnitOfWork(false)]
        //[HttpPost]
        //public async Task<LinuxStateDto> UpdateItemAsync(LinuxStateUpdateDto input)
        //{
        //    var info = _repository.Where(x => x.Id == input.Id).AsNoTracking().FirstOrDefault();
        //    if (info == null || info == default)
        //    {
        //        throw new UserFriendlyException("需要查询的信息不存在", "404");
        //    }
        //    var newu = ObjectMapper.Map<LinuxStateUpdateDto, LinuxState>(input, info);
        //    var updated = await _repository.UpdateAsync(newu);
        //    var backinfo = ObjectMapper.Map<LinuxState, LinuxStateDto>(updated);
        //    return backinfo;
        //}

        ///// <summary>
        ///// 删除Linux机器的CPU和内存情况软删除
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<int> DeleteItemById(int id)
        //{
        //    var aione = await _repository.GetAsync(xy => xy.Id == id);//.FirstOrDefault();
        //    if (aione != default)
        //    {
        //        //await _repository.DeleteAsync(aione);
        //    }
        //    return 1;
        //}

    }
}
