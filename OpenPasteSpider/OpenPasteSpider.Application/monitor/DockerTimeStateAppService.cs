﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OpenPasteSpider.monitor;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace OpenPasteSpider.monitormodel
{

    /// <summary>
    /// 非10秒的数据
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class DockerTimeStateAppService : OpenPasteSpiderAppService
    {

        //private readonly IRepository<DockerTimeState, long> _repository;


        private readonly IOpenPasteSpiderDbContext _dbContext;


        public DockerTimeStateAppService(IOpenPasteSpiderDbContext dbContext
        )
        {
            _dbContext = dbContext;
            //_repository = repository;
        }
        /// <summary>
        /// 按页获取非10秒的数据
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<DockerTimeStateListDto>> GetListAsync(InputSearch input)
        {

            var query = _dbContext.DockerTimeState.Where(t => 1 == 1)
                .WhereIf(!String.IsNullOrEmpty(input.Word), x => x.DockerName == input.Word)
                .WhereIf(!String.IsNullOrEmpty(input.Word2), x => x.DockerName == input.Word)
                .AsNoTracking()
                .OrderBy(xy => xy.Id);
            PagedResultDto<DockerTimeStateListDto> pagedResultDto = new PagedResultDto<DockerTimeStateListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(input.page, input.size).ToListAsync();
            var temList = ObjectMapper.Map<List<DockerTimeState>, List<DockerTimeStateListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 根据时间读取报表数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<DockerTimeStateListDto>> ListCharts(InputMonitorSearch input)
        {

            var query = _dbContext.DockerTimeState
                .WhereIf(!String.IsNullOrEmpty(input.Word), x => x.DockerName == input.Word)
                .Where(x => x.DataDate >= input.datestart && x.DataDate <= input.dateend)
                .WhereIf(input.objectid != 0, x => x.LinuxId == input.objectid)
                .AsNoTracking()
                .OrderBy(xy => xy.DataDate);
            PagedResultDto<DockerTimeStateListDto> pagedResultDto = new PagedResultDto<DockerTimeStateListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.ToListAsync();
            var temList = ObjectMapper.Map<List<DockerTimeState>, List<DockerTimeStateListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        ///// <summary>
        ///// 根据ID获取单项非10秒的数据
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public DockerTimeStateDto GetByIdAsync(int id)
        //{
        //    var query = _repository.Where(t => t.Id == id)
        //        .FirstOrDefault();
        //    var temList = ObjectMapper.Map<DockerTimeState, DockerTimeStateDto>(query);
        //    return temList;
        //}

        ///// <summary>
        ///// 根据ID获取待更新单项信息非10秒的数据
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public DockerTimeStateUpdateDto GetInfoForUpdateAsync(int id)
        //{
        //    var query = _repository.Where(t => t.Id == id)
        //        .FirstOrDefault();
        //    var temList = ObjectMapper.Map<DockerTimeState, DockerTimeStateUpdateDto>(query);
        //    return temList;
        //}


        ///// <summary>
        ///// 添加一个非10秒的数据
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[UnitOfWork(false)]
        //[HttpPost]
        //public async Task<DockerTimeStateDto> CreateItemAsync(DockerTimeStateAddDto input)
        //{

        //    var newu = ObjectMapper.Map<DockerTimeStateAddDto, DockerTimeState>(input);
        //    //添加自定义
        //    var updated = await _repository.InsertAsync(newu, true);
        //    var backinfo = ObjectMapper.Map<DockerTimeState, DockerTimeStateDto>(updated);
        //    return backinfo;
        //}
        ///// <summary>
        ///// 更新一个非10秒的数据
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[UnitOfWork(false)]
        //[HttpPost]
        //public async Task<DockerTimeStateDto> UpdateItemAsync(DockerTimeStateUpdateDto input)
        //{
        //    var info = _repository.Where(x => x.Id == input.Id).AsNoTracking().FirstOrDefault();
        //    if (info == null || info == default)
        //    {
        //        throw new UserFriendlyException("需要查询的信息不存在", "404");
        //    }
        //    var newu = ObjectMapper.Map<DockerTimeStateUpdateDto, DockerTimeState>(input, info);
        //    var updated = await _repository.UpdateAsync(newu);
        //    var backinfo = ObjectMapper.Map<DockerTimeState, DockerTimeStateDto>(updated);
        //    return backinfo;
        //}

        ///// <summary>
        ///// 删除非10秒的数据软删除
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<int> DeleteItemById(int id)
        //{
        //    var aione = await _repository.GetAsync(xy => xy.Id == id);//.FirstOrDefault();
        //    if (aione != default)
        //    {
        //        //await _repository.DeleteAsync(aione);
        //    }
        //    return 1;
        //}

    }
}
