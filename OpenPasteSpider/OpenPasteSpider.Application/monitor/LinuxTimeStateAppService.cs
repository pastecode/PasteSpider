﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OpenPasteSpider.monitor;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace OpenPasteSpider.monitormodel
{

    /// <summary>
    /// 服务器的小时监控数据
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class LinuxTimeStateAppService : OpenPasteSpiderAppService
    {

        //private readonly IRepository<LinuxTimeState, long> _repository;
        private readonly IOpenPasteSpiderDbContext _dbContext;

        public LinuxTimeStateAppService(IOpenPasteSpiderDbContext dbContext
        )
        {
            _dbContext = dbContext;
            //_repository = repository;
        }
        /// <summary>
        /// 按页获取服务器的小时监控数据
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<LinuxTimeStateListDto>> GetListAsync(InputSearch input)
        {

            var query = _dbContext.LinuxTimeState.Where(t => 1 == 1)
                .WhereIf(input.objectid != 0, x => x.LinuxId == input.objectid)
                .OrderByDescending(xy => xy.Id);
            PagedResultDto<LinuxTimeStateListDto> pagedResultDto = new PagedResultDto<LinuxTimeStateListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(input.page, input.size).ToListAsync();
            var temList = ObjectMapper.Map<List<LinuxTimeState>, List<LinuxTimeStateListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 读取图表数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<LinuxTimeStateListDto>> ListCharts(InputMonitorSearch input)
        {

            var query = _dbContext.LinuxTimeState.Where(t => 1 == 1).WhereIf(input.objectid != 0, x => x.LinuxId == input.objectid)
                .Where(x => x.DataDate >= input.datestart && x.DataDate <= input.dateend)
                .AsNoTracking()
                .OrderBy(xy => xy.DataDate);
            PagedResultDto<LinuxTimeStateListDto> pagedResultDto = new PagedResultDto<LinuxTimeStateListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.ToListAsync();
            var temList = ObjectMapper.Map<List<LinuxTimeState>, List<LinuxTimeStateListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        ///// <summary>
        ///// 根据ID获取单项服务器的小时监控数据
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public LinuxTimeStateDto GetByIdAsync(int id)
        //{
        //    var query = _repository.Where(t => t.Id == id)
        //        .FirstOrDefault();
        //    var temList = ObjectMapper.Map<LinuxTimeState, LinuxTimeStateDto>(query);
        //    return temList;
        //}

        ///// <summary>
        ///// 根据ID获取待更新单项信息服务器的小时监控数据
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public LinuxTimeStateUpdateDto GetInfoForUpdateAsync(int id)
        //{
        //    var query = _repository.Where(t => t.Id == id)
        //        .FirstOrDefault();
        //    var temList = ObjectMapper.Map<LinuxTimeState, LinuxTimeStateUpdateDto>(query);
        //    return temList;
        //}


        ///// <summary>
        ///// 添加一个服务器的小时监控数据
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[UnitOfWork(false)]
        //[HttpPost]
        //public async Task<LinuxTimeStateDto> CreateItemAsync(LinuxTimeStateAddDto input)
        //{

        //    var newu = ObjectMapper.Map<LinuxTimeStateAddDto, LinuxTimeState>(input);
        //    //添加自定义
        //    var updated = await _repository.InsertAsync(newu, true);
        //    var backinfo = ObjectMapper.Map<LinuxTimeState, LinuxTimeStateDto>(updated);
        //    return backinfo;
        //}
        ///// <summary>
        ///// 更新一个服务器的小时监控数据
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[UnitOfWork(false)]
        //[HttpPost]
        //public async Task<LinuxTimeStateDto> UpdateItemAsync(LinuxTimeStateUpdateDto input)
        //{
        //    var info = _repository.Where(x => x.Id == input.Id).AsNoTracking().FirstOrDefault();
        //    if (info == null || info == default)
        //    {
        //        throw new UserFriendlyException("需要查询的信息不存在", "404");
        //    }
        //    var newu = ObjectMapper.Map<LinuxTimeStateUpdateDto, LinuxTimeState>(input, info);
        //    var updated = await _repository.UpdateAsync(newu);
        //    var backinfo = ObjectMapper.Map<LinuxTimeState, LinuxTimeStateDto>(updated);
        //    return backinfo;
        //}

        ///// <summary>
        ///// 删除服务器的小时监控数据软删除
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<int> DeleteItemById(int id)
        //{
        //    var aione = await _repository.GetAsync(xy => xy.Id == id);//.FirstOrDefault();
        //    if (aione != default)
        //    {
        //        //await _repository.DeleteAsync(aione);
        //    }
        //    return 1;
        //}

    }
}
