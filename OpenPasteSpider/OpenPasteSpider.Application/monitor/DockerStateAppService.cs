﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OpenPasteSpider.monitor;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace OpenPasteSpider.monitormodel
{

    /// <summary>
    /// Docker的监控记录
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class DockerStateAppService : OpenPasteSpiderAppService
    {

        //private readonly IRepository<DockerState, long> _repository;


        private readonly IOpenPasteSpiderDbContext _dbContext;


        public DockerStateAppService(IOpenPasteSpiderDbContext dbContext)
        {
            _dbContext = dbContext;
            //_repository = repository;
        }
        /// <summary>
        /// 按页获取Docker的监控记录
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<DockerStateListDto>> GetListAsync(InputSearch input)
        {

            var query = _dbContext.DockerState.Where(t => 1 == 1)
                .WhereIf(!String.IsNullOrEmpty(input.Word), x => x.DockerName == input.Word)
                .WhereIf(!String.IsNullOrEmpty(input.Word2), x => x.DockerName == input.Word)
                .AsNoTracking()
                .OrderBy(xy => xy.Id);
            PagedResultDto<DockerStateListDto> pagedResultDto = new PagedResultDto<DockerStateListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(input.page, input.size).ToListAsync();
            var temList = ObjectMapper.Map<List<DockerState>, List<DockerStateListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 根据时间读取报表数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<DockerStateListDto>> ListCharts([FromQuery] InputMonitorSearch input)
        {

            var query = _dbContext.DockerState
                .WhereIf(!String.IsNullOrEmpty(input.Word), x => x.DockerName == input.Word)
                .WhereIf(input.objectid != 0, x => x.LinuxId == input.objectid)
                .Where(x => x.DataDate >= input.datestart && x.DataDate <= input.dateend)
                .OrderBy(xy => xy.DataDate);
            PagedResultDto<DockerStateListDto> pagedResultDto = new PagedResultDto<DockerStateListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.ToListAsync();
            var temList = ObjectMapper.Map<List<DockerState>, List<DockerStateListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 根据ID获取单项Docker的监控记录
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public DockerStateDto GetByIdAsync(int id)
        {
            var query = _dbContext.DockerState.Where(t => t.Id == id).AsNoTracking()
                .FirstOrDefault();
            var temList = ObjectMapper.Map<DockerState, DockerStateDto>(query);
            return temList;
        }

        ///// <summary>
        ///// 根据ID获取待更新单项信息Docker的监控记录
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public DockerStateUpdateDto GetInfoForUpdateAsync(int id)
        //{
        //    var query = _dbContext.DockerState.Where(t => t.Id == id).AsNoTracking()
        //        .FirstOrDefault();
        //    var temList = ObjectMapper.Map<DockerState, DockerStateUpdateDto>(query);
        //    return temList;
        //}






    }
}
