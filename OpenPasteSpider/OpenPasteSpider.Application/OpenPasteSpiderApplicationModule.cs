﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace OpenPasteSpider
{
    [DependsOn(
        typeof(OpenPasteSpiderDomainModule),
        typeof(OpenPasteSpiderApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule)
        )]
    public class OpenPasteSpiderApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {

            //Console.WriteLine("---OpenPasteSpiderApplicationModule---");

            context.Services.AddAutoMapperObjectMapper<OpenPasteSpiderApplicationModule>();
            Configure<AbpAutoMapperOptions>(options =>
            {
                //是否一一映射
                options.AddMaps<OpenPasteSpiderApplicationModule>(validate: false);

            });





            //context.Services.AddSingleton<IConnectionMultiplexer, ConnectionMultiplexer>();
            //context.Services.AddSingleton<IConnectionMultiplexer>(a =>
            //{
            //    string configuration = "{0},$UNLINK=,abortConnect=false,defaultDatabase={1},ssl=false,ConnectTimeout={2},allowAdmin=true,connectRetry={3},password={4}";
            //    ConnectionMultiplexer connectionMultiplexer = ConnectionMultiplexer.Connect(string.Format(configuration, "127.0.0.1:6379", 0, 1800, 3, "123456"));
            //    return connectionMultiplexer;
            //});



            //Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

        }
    }
}
