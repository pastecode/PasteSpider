﻿using Renci.SshNet;

namespace OpenPasteSpider
{
    /// <summary>
    /// 链接标记信息
    /// </summary>
    public class LinkSSHClient : SshClient
    {

        public LinkSSHClient(ConnectionInfo connectionInfo) : base(connectionInfo)
        {
        }

        public LinkSSHClient(string host, string username, string password) : base(host, username, password)
        {
        }

        public LinkSSHClient(string host, string username, params PrivateKeyFile[] keyFiles) : base(host, username, keyFiles)
        {
        }

        public LinkSSHClient(string host, int port, string username, string password) : base(host, port, username, password)
        {
        }

        public LinkSSHClient(string host, int port, string username, params PrivateKeyFile[] keyFiles) : base(host, port, username, keyFiles)
        {
        }

        /// <summary>
        /// 链接是否确认了
        /// </summary>
        public bool LinkSigned { get; set; } = false;

        /// <summary>
        /// 是否正在处理任务中
        /// </summary>
        public bool working { get; set; } = false;

        /// <summary>
        /// 是否触发了异常，验证失败，超时链接等
        /// </summary>
        public bool exception { get; set; }

    }
}
