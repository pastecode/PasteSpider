﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace OpenPasteSpider.projectmodel
{

    /// <summary>
    /// 项目
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class ProjectInfoAppService : OpenPasteSpiderAppService
    {

        private IOpenPasteSpiderDbContext _dbContext;

        private readonly PublicModelHelper _modelHelper;

        public ProjectInfoAppService(IOpenPasteSpiderDbContext dbContext, PublicModelHelper modelHelper)
        {
            _dbContext = dbContext;
            _modelHelper = modelHelper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<ProjectInfoListDto>> GetListAsync(int page = 1, int size = 20)
        {

            var query = _dbContext.ProjectInfo.Where(t => 1 == 1)
            .OrderByDescending(xy => xy.Id);

            var pagedResultDto = new PagedResultDto<ProjectInfoListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Page(page, size).ToListAsync();
            var temList = ObjectMapper.Map<List<ProjectInfo>, List<ProjectInfoListDto>>(userList);
            pagedResultDto.Items = temList;
            return pagedResultDto;
        }

        /// <summary>
        /// 读取可用的项目列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<ProjectInfoListDto>> ReadProject()
        {

            var query =await _dbContext.ProjectInfo.Where(t => t.IsEnable).OrderByDescending(xy => xy.Id).ToListAsync();
            var temList = ObjectMapper.Map<List<ProjectInfo>, List<ProjectInfoListDto>>(query);
            return temList;
        }

        /// <summary>
        /// 根据ID获取单项项目
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ProjectInfoDto GetByIdAsync(int id)
        {
            var query = _dbContext.ProjectInfo.Where(t => t.Id == id)
            .FirstOrDefault();
            var temList = ObjectMapper.Map<ProjectInfo, ProjectInfoDto>(query);
            return temList;
        }

        /// <summary>
        /// 根据ID获取待更新单项信息项目
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ProjectInfoUpdateDto GetInfoForUpdateAsync(int id)
        {
            var query = _dbContext.ProjectInfo.Where(t => t.Id == id)
            .FirstOrDefault();
            var temList = ObjectMapper.Map<ProjectInfo, ProjectInfoUpdateDto>(query);
            return temList;
        }


        /// <summary>
        /// 添加一个项目
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ProjectInfoDto> CreateItemAsync(ProjectInfoAddDto input)
        {

            var newu = ObjectMapper.Map<ProjectInfoAddDto, ProjectInfo>(input);
            newu.IsEnable = true;            //添加自定义
            _dbContext.Add(newu);
            await _dbContext.SaveChangesAsync();
            //var updated = await _repository.InsertAsync(newu,true);
            var backinfo = ObjectMapper.Map<ProjectInfo, ProjectInfoDto>(newu);
            return backinfo;
        }
        /// <summary>
        /// 更新一个项目
        ///</summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ProjectInfoDto> UpdateItemAsync(ProjectInfoUpdateDto input)
        {
            var info = await _dbContext.ProjectInfo.Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            if (info == null || info == default)
            {
                throw new UserFriendlyException("需要查询的信息不存在", "404");
            }
            ObjectMapper.Map<ProjectInfoUpdateDto, ProjectInfo>(input, info);
            //var updated = await _repository.UpdateAsync(newu);
            await _dbContext.SaveChangesAsync();
            var backinfo = ObjectMapper.Map<ProjectInfo, ProjectInfoDto>(info);
            await _modelHelper.ItemProjectClean(input.Id);
            return backinfo;
        }
    }
}
