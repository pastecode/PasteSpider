﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Domain.Repositories;

namespace OpenPasteSpider.projectmodel
{

    /// <summary>
    /// linux待执行命令
    ///</summary>
    [TypeFilter(typeof(RoleAttribute), Arguments = new object[] { "data", "view" })]
    public class PlanItemAppService : OpenPasteSpiderAppService
    {

        private IOpenPasteSpiderDbContext _dbContext;
        public PlanItemAppService(IOpenPasteSpiderDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="size"></param>
        /// <param name="planid"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PagedResultDto<PlanItemListDto>> GetListAsync(int page = 1, int size = 20, int planid = 0)
        {

            var query = _dbContext.PlanItem.Where(t => 1 == 1)
                .WhereIf(planid != 0, x => x.PlanInfoId == planid);
            var pagedResultDto = new PagedResultDto<PlanItemListDto>();
            pagedResultDto.TotalCount = await query.CountAsync();
            var userList = await query.Select(x => new PlanItemListDto
            {
                Id = x.Id,
                ActionType = x.ActionType,
                AppId = x.AppId,
                Command = x.Command,
                CreateDate = x.CreateDate,
                Durtion = x.Durtion,
                ExecState = x.ExecState,
                ExitCode = x.ExitCode,
                PlanInfoId = x.PlanInfoId,
                RunIndex = x.RunIndex,
                StrictMode = x.StrictMode,
                Target = x.Target
            }).OrderBy(x=>x.RunIndex).Page(page, size).ToListAsync();
            //var temList = ObjectMapper.Map<List<PlanItem>, List<PlanItemListDto>>(userList);
            pagedResultDto.Items = userList;
            return pagedResultDto;
        }

        /// <summary>
        /// 根据ID获取单项linux待执行命令
        ///</summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public PlanItemDto GetByIdAsync(int id)
        {
            var query = _dbContext.PlanItem.Where(t => t.Id == id).AsNoTracking().FirstOrDefault();
            var temList = ObjectMapper.Map<PlanItem, PlanItemDto>(query);
            return temList;
        }

        ///// <summary>
        ///// 根据ID获取待更新单项信息linux待执行命令
        /////</summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public PlanItemUpdateDto GetInfoForUpdateAsync(int id)
        //{
        //    var query = _dbContext.PlanItem.Where(t => t.Id == id)
        //    .FirstOrDefault();
        //    var temList = ObjectMapper.Map<PlanItem, PlanItemUpdateDto>(query);
        //    return temList;
        //}


        ///// <summary>
        ///// 添加一个linux待执行命令
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<PlanItemDto> CreateItemAsync(PlanItemAddDto input)
        //{

        //    var newu = ObjectMapper.Map<PlanItemAddDto, PlanItem>(input);
        //    newu.CreateDate = DateTime.Now;            //添加自定义
        //    _dbContext.Add(newu);
        //    await _dbContext.SaveChangesAsync();
        //    //var updated = await _repository.InsertAsync(newu,true);
        //    var backinfo = ObjectMapper.Map<PlanItem, PlanItemDto>(newu);
        //    return backinfo;
        //}
        ///// <summary>
        ///// 更新一个linux待执行命令
        /////</summary>
        ///// <param name="input"></param>
        ///// <returns></returns>
        //[HttpPost]
        //public async Task<PlanItemDto> UpdateItemAsync(PlanItemUpdateDto input)
        //{
        //    var info = await _dbContext.PlanItem.Where(x => x.Id == input.Id)
        //        .FirstOrDefaultAsync();
        //    if (info == null || info == default)
        //    {
        //        throw new UserFriendlyException("需要查询的信息不存在", "404");
        //    }
        //    ObjectMapper.Map<PlanItemUpdateDto, PlanItem>(input, info);
        //    //var updated = await _repository.UpdateAsync(newu);
        //    await _dbContext.SaveChangesAsync();
        //    var backinfo = ObjectMapper.Map<PlanItem, PlanItemDto>(info);
        //    return backinfo;
        //}
    }
}
